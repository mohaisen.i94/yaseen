import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/bn_gradiant.dart';
import 'package:yaseen_app/screens/landing_page.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class OnBoarding extends StatefulWidget {
  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  late PageController _pageController;
  int _currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BNGradiant(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            PageView(
              controller: _pageController,
              onPageChanged: (int currentPage) {
                setState(() {
                  _currentPage = currentPage;
                });
              },
              children: [
                buildPageView(context, 'كيف الحال'),
                buildPageView(context, 'صلّ على النبي'),
                buildPageView(context, 'لا إله إلا الله')
              ],
            ),
            Positioned(
              bottom: 30,
              left: 150,
              right: 150,
              child: CostumeContainer(
                function: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LandingPage(),
                      ));
                },
                color: Color(0xFFFFC000),
                radiusBottom: 30,
                radiusTop: 30,
                height: 55,
                child: Text(
                  'التالي',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.black),
                ),
              ),
            ),
            Positioned(
                bottom: 200,
                left: 0,
                right: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        color: _currentPage == 0
                            ? Colors.orange
                            : Colors.orange.shade200,
                        shape: BoxShape.circle,
                      ),
                    ),
                    SizedBox(width: 15),
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        color: _currentPage == 1
                            ? Colors.orange
                            : Colors.orange.shade200,
                        shape: BoxShape.circle,
                      ),
                    ),
                    SizedBox(width: 15),
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        color: _currentPage == 2
                            ? Colors.orange
                            : Colors.orange.shade200,
                        shape: BoxShape.circle,
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  Stack buildPageView(BuildContext context, text) {
    return Stack(
      children: [
        const CostumeContainer(
            marginHorizontal: 30,
            marginVerticalBottom: 50,
            radiusBottom: 40,
            color: Color(0xFFFEFFEC)),

        Positioned(
          top: 100,
          left: 150,
          right: 150,
          child: SizedBox(
              child: Image.asset(
            'images/imgimg.png',
            fit: BoxFit.cover,
          )),
        ),
        Positioned(
          top: 400,
          left: 150,
          right: 150,
          child: Text(
            'أهلا ومرحبا بكم',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold),
          ),
        ),
        Positioned(
          top: 500,
          left: 0,
          right: 0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                text,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ],
          ),
        ),

        // Expanded(
        //   child: PageView(
        //     onPageChanged: (int selectedPage) {
        //       setState(() {
        //         _currentPage = selectedPage;
        //       });
        //     },
        //     controller: _pageController,
        //     children: [
        //       Text('كيف الحال'),
        //       Text('صل على النبي'),
        //       Text('لاإله إلا الله'),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
