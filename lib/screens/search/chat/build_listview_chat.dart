import 'package:flutter/material.dart';

import 'message_bubble.dart';

class Message {
  String message;
  bool isMe;
  Message({required this.message, required this.isMe});
}

class BuildListViewChat extends StatelessWidget {
  List docs = [
    Message(message: ',nmnbvbcv', isMe: true),
    Message(message: ',iuyyff', isMe: false),
    Message(message: ',mjhgksjdhjdfieuisdnjksdhjhiur', isMe: true),
    Message(
        message:
            ',nvmnvmkjhkdfjdhfkjskdjdhkjghjghfjgfhghghghghjgjhghhgfgfgjjgggggggggggggggggggggggggghjjjjj',
        isMe: true),
    Message(message: ',jkjhfieureisjlskjfkhjgh', isMe: false),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView.builder(
          reverse: true,
          itemCount: docs.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                print('hello');
              },
              child: MessageBubble(
                message: docs[index].message,
                isMe: docs[index].isMe,
              ),
            );
          }),
    );
  }
}
