import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/bn_gradiant.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class ImageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BNGradiant(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          child: GridView.builder(
            clipBehavior: Clip.antiAlias,
            padding: EdgeInsets.all(20),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 10,
                mainAxisSpacing: 15,
                crossAxisCount: 2,
                childAspectRatio: 1.5),
            itemBuilder: (context, index) => Container(
              clipBehavior: Clip.antiAlias,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  SizedBox(
                    height: 250,
                    width: 250,
                    child: Image.asset(
                      'images/img.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        alignment: Alignment.center,
                        width: 70,
                        child: Text(
                          'حذف',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white),
                        ),
                        decoration: const BoxDecoration(
                            color: Color(0xFFCF1D1F),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10),
                                topRight: Radius.circular(5),
                                bottomRight: Radius.circular(5))),
                      ),
                    ),
                  )
                ],
              ),
            ),
            itemCount: 12,
          ),
        ),
      ),
    );
  }
}
