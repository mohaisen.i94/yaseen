import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class LandingPage2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            child: Container(
              height: 740,
              width: 700,
              margin: EdgeInsets.only(bottom: 300, right: 200),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [Color(0xFFF1F8ED), Color(0xFFF6E5B9)],
                      begin: Alignment.topRight,
                      end: Alignment.bottomRight)),
            ),
          ),
          Column(
            children: [
              Image.asset('images/imgimg.png'),
              BuildTextField(text: 'اسم المستخدم', height: 55),
              SizedBox(height: 10),
              BuildTextField(text: 'اسم المستخدم', height: 55),
              SizedBox(height: 10),
              Row(
                children: [
                  Text(
                    'تذكرني',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.check_circle,
                        color: Colors.orange,
                      )),
                  Spacer(),
                  CostumeContainer(
                    height: 50,
                    width: 100,
                    color: Colors.orange,
                    child: Text(
                      'دخول',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                    radiusTop: 10,
                    radiusBottom: 10,
                  )
                ],
              ),
              CostumeContainer(
                height: 60,
                radiusBottom: 10,
                radiusTop: 10,
                color: Color(0xFFFFC000),
                child: Text(
                  'المكتبة والأنشطة',
                  style: Theme.of(context)
                      .textTheme
                      .headline2!
                      .copyWith(color: Colors.black),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: CostumeContainer(
                      height: 50,
                      color: Colors.orange,
                      child: Text(
                        'الأهل',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white),
                      ),
                      radiusTop: 10,
                      radiusBottom: 10,
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: CostumeContainer(
                      height: 50,
                      color: Colors.orange,
                      child: Text(
                        'المحفظ',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white),
                      ),
                      radiusTop: 10,
                      radiusBottom: 10,
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: CostumeContainer(
                      height: 50,
                      color: Colors.orange,
                      child: Text(
                        'الإدارة',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white),
                      ),
                      radiusTop: 10,
                      radiusBottom: 10,
                    ),
                  ),
                ],
              ),
              Text(
                'مركز الشهيد أحمد ياسين لتحفيظ القرآن الكريم',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.black, fontSize: 20),
              )
            ],
          ),
        ],
      ),
    );
  }
}
