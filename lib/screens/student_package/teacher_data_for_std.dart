import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class TeacherDataForStd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات المحفظ للطالب',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: CostumeContainer(
          color: Colors.white,
          radiusTop: 20,
          radiusBottom: 20,
          padding: 20,
          child: CostumeContainer(
            color: Color(0xFFF1F8ED),
            radiusBottom: 20,
            radiusTop: 20,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(),
                CircleAvatar(
                  backgroundColor: Theme.of(context).primaryColor,
                  radius: 100,
                ),
                SizedBox(height: 50),
                Text(
                  'المحفظ',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                Text(
                  'عبد الرحمن عبد الكريم عبد الله',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Color(0xFF563319),
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                SizedBox(height: 30),
                Text(
                  'رقم الجوال',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                Text(
                  '053333333',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Color(0xFF563319),
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
                Spacer(flex: 3),
                Text(
                  'إظهار المحفظ الأساسي والمساعد',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.black, fontSize: 25),
                ),
                Spacer(flex: 2),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
