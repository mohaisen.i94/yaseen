import 'package:flutter/material.dart';

//
class CostumeImage extends StatelessWidget {
  CostumeImage({required this.radius, this.marginVir = 20});
  double radius;
  double marginVir;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: marginVir),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        width: radius,
        height: radius,
        clipBehavior: Clip.antiAlias,
        child: Image.asset(
          'images/img.png',
          fit: BoxFit.cover,
        ));
  }
}
