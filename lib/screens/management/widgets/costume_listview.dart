import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CostumeListView extends StatelessWidget {
  final data;
  CostumeListView({required this.data});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: data.length,
      itemBuilder: (context, index) => Row(
        children: [
          Expanded(
              child: BuildTextField(
            text: data[index].subText,
            height: 55,
          )),
          CostumeContainer(
              width: 140,
              height: 55,
              child: Text(
                data[index].mainText,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
              color: Theme.of(context).primaryColor)
        ],
      ),
    );
  }
}
