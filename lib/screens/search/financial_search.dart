import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class FinancialSearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'تسجيل المصروفات',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
        child: Column(
          children: [
            CostumeContainer(
              child: Text('اليوم والتاريخ',
                  style: Theme.of(context).textTheme.bodyText1),
              color: Colors.white,
              height: 45,
            ),
            CostumeContainer(
              child:
                  Text('الصنف', style: Theme.of(context).textTheme.bodyText1),
              color: Colors.white,
              height: 45,
            ),
            Row(
              children: [
                Expanded(
                    child: CostumeContainer(
                  child: Text('الكمية',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  height: 45,
                )),
                SizedBox(width: 5),
                Expanded(
                    child: CostumeContainer(
                  child: Text('السعر',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  height: 45,
                )),
              ],
            ),
            CostumeContainer(
              child: Text(
                'موافق',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
              color: Theme.of(context).primaryColor,
              height: 45,
              radiusTop: 5,
              radiusBottom: 5,
              marginVerticalBottom: 20,
            ),
            CostumeContainer(child: Text(''), color: Colors.white)
          ],
        ),
      ),
    );
  }
}
