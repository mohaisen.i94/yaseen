import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/bn_gradiant.dart';
import 'package:yaseen_app/screens/image_screen.dart';
import 'package:yaseen_app/screens/landing_page2.dart';
import 'package:yaseen_app/screens/students/enter_ads.dart';
import 'package:yaseen_app/screens/students/library.dart';
import 'package:yaseen_app/screens/students/students_screen.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/landing_second_widget.dart';
import 'package:yaseen_app/widgets/landing_widget.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BNGradiant(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            backGround(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Column(
                children: [
                  landingBody(
                      studentFunction: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EnterAds(),
                          )),
                      newsFunction: () {},
                      context: context,
                      date: '20/20/20',
                      name: 'عبد الله عبد الرحمن عبد السميع',
                      newsDate: '20/20/20',
                      newsTitle: 'دورة أحكام',
                      newsText:
                          'تفاصيل الخبر  تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر'),
                  SizedBox(height: 15),
                  orangeButton('القرآن الكريم', 30, () {}),
                  orangeButton(
                    'المكتبة',
                    30,
                    () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LibraryScreen(),
                        )),
                  ),
                  Row(
                    children: [
                      SizedBox(width: 28),
                      Expanded(
                          child: orangeButton(
                        'صور',
                        2,
                        () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ImageScreen(),
                            )),
                      )),
                      // SizedBox(width: 1),
                      Expanded(
                        child: orangeButton(
                          'الحفظة',
                          2,
                          () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StudentScreen(),
                              )),
                        ),
                      ),
                      SizedBox(width: 28),
                    ],
                  ),
                  SizedBox(height: 5),
                  CostumeContainer(
                      function: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LandingPage2(),
                          )),
                      width: 170,
                      height: 60,
                      radiusBottom: 20,
                      radiusTop: 20,
                      child: Text(
                        'تسجيل الدخول',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      color: Colors.orange)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget orangeButton(text, double margin, function) {
    return CostumeContainer(
      function: function,
      child: Text(text),
      color: Color(0xFFF4EDCF),
      borderColor: Colors.orange,
      height: 55,
      borderWidth: 1,
      radiusTop: 10,
      radiusBottom: 10,
      marginHorizontal: margin,
      marginVerticalBottom: 5,
      shadow: false,
    );
  }

  Widget backGround() {
    return Stack(
      children: [
        const CostumeContainer(
            marginHorizontal: 30,
            marginVerticalBottom: 37,
            radiusBottom: 40,
            color: Color(0xFFFEFFEC)),
        Container(
          height: 400,
          margin: EdgeInsets.symmetric(horizontal: 30),
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Color(0xFFDBAE1A).withOpacity(.7),
            Color(0xFFF2F5E4)
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        ),
      ],
    );
  }

  Widget landingBody({
    required BuildContext context,
    required String date,
    required String name,
    required String newsDate,
    required String newsTitle,
    required String newsText,
    required Function() studentFunction,
    required Function() newsFunction,
  }) {
    return Column(
      children: [
        SizedBox(
            height: 100,
            child: Image.asset(
              'images/star.png',
              fit: BoxFit.cover,
            )),
        Text(
          'المميزون خلال الشهر ',
          style: Theme.of(context).textTheme.headline2!.copyWith(
                color: Color(0xFFB26500),
                fontWeight: FontWeight.w200,
                fontSize: 25,
              ),
        ),
        SizedBox(
          height: 190,
          child: ListView.builder(
            padding: EdgeInsets.all(5),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => InkWell(
              onTap: studentFunction,
              child: LandingWidget(
                date: date,
                name: name,
              ),
            ),
            itemCount: 3,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          'الأخبار والإعلانات ',
          style: Theme.of(context).textTheme.headline2!.copyWith(
                color: Color(0xFFB26500),
                fontWeight: FontWeight.w200,
                fontSize: 25,
              ),
        ),
        SizedBox(
          height: 170,
          child: ListView.builder(
            padding: EdgeInsets.all(5),
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (context, index) => InkWell(
              onTap: newsFunction,
              child: LandingSecondWidget(
                  date: newsDate, title: newsTitle, text: newsText),
            ),
          ),
        ),
      ],
    );
  }
}
