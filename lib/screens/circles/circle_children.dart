import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CircleChildren extends StatefulWidget {
  CircleChildren({Key? key}) : super(key: key);

  @override
  State<CircleChildren> createState() => _CircleChildrenState();
}

class _CircleChildrenState extends State<CircleChildren> {
  var status;

  bool openWidget = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    status = 'حاضر';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'حلقة البراعم',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 25),
        child: Column(
          children: [
            BuildAbsenceWidget(context: context, absence: '10', presence: '25'),
            BuildRichTextDayAndDate(
                context: context, day: 'السبت', date: "24/12/20"),
            Expanded(
              child: BuildRowListView(
                visible: status == 'حاضر',
                status: status,
                name: 'اسم الطالب',
              ) /* buildListViewCostume(context,status == 'حاضر',status,openWidget,_index)*/,
            ),
          ],
        ),
      ),
    );
  }
}

class BuildRowListView extends StatefulWidget {
  final bool visible;
  final String status;
  final String name;
  final bool starColor;
  final bool visibleStar;
  final Function()? function;
  const BuildRowListView({
    Key? key,
    required this.visible,
    required this.status,
    required this.name,
    this.starColor = true,
    this.visibleStar = true,
    this.function,
  }) : super(key: key);

  @override
  State<BuildRowListView> createState() => _BuildRowListViewState();
}

class _BuildRowListViewState extends State<BuildRowListView> {
  bool openWidget = false;

  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => Column(
        children: [
          Row(
            children: [
              CostumeContainer(
                child: Text(
                  widget.status,
                  style: (widget.status == 'غائب' || widget.status == 'مأذون')
                      ? Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white)
                      : Theme.of(context).textTheme.bodyText1,
                ),
                width: 130,
                height: 60,
                marginVertical: 0,
                borderColor: const Color(0xFFC8C8C8),
                color: (index % 2 == 0 &&
                        (widget.status != 'غائب' || widget.status != 'مأذون'))
                    ? const Color(0xFFDCFCC6)
                    : (widget.status == 'غائب')
                        ? const Color(0xFFBB2D2D)
                        : (widget.status == 'مأذون')
                            ? Colors.orange
                            : Colors.white,
              ),
              Expanded(
                  child: CostumeContainer(
                child: Row(
                  children: [
                    Visibility(
                      visible: widget.visible,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            openWidget = !openWidget;
                            _index = index;
                          });
                        },
                        child: Container(
                          height: 60,
                          child: const Icon(
                            Icons.arrow_drop_down_sharp,
                            size: 20,
                          ),
                          decoration: BoxDecoration(
                              color: const Color(0xFFE2EBDD),
                              borderRadius: BorderRadius.circular(2)),
                        ),
                      ),
                    ),
                    const Spacer(),
                    TextButton(
                      onPressed: widget.function,
                      child: Text(
                        widget.name,
                        style: (widget.status == 'غائب' ||
                                widget.status == 'مأذون')
                            ? Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(color: Colors.white)
                            : Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                    const SizedBox(width: 15),
                    widget.visibleStar // أضف الشرط الذي يحقق غاية وجود هذه الايقونة
                        ? Icon(
                            Icons.star,
                            color:
                                widget.starColor ? Colors.yellow : Colors.grey,
                          )
                        : Container(),
                    const SizedBox(width: 15),
                    CostumeImage(radius: 40, marginVir: 0),
                    const SizedBox(width: 10),
                  ],
                ),
                padding: 5,
                height: 60,
                shadow: false,
                marginVertical: .2,
                borderColor: const Color(0xFFC8C8C8),
                color: (index % 2 == 0 &&
                        (widget.status != 'غائب' || widget.status != 'مأذون'))
                    ? const Color(0xFFDCFCC6)
                    : (widget.status == 'غائب')
                        ? const Color(0xFFBB2D2D)
                        : (widget.status == 'مأذون')
                            ? Colors.orange
                            : Colors.white,
              )),
            ],
          ),
          Visibility(
              visible: openWidget && _index == index,
              child: CostumeContainer(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                      onPressed: () {},
                      child: Text(' اتصال ',
                          style: Theme.of(context).textTheme.bodyText1),
                    ),
                    const Text('|'),
                    TextButton(
                      onPressed: () {},
                      child: Text('الخطة',
                          style: Theme.of(context).textTheme.bodyText1),
                    ),
                    const Text('|'),
                    TextButton(
                      onPressed: () {},
                      child: Text('السجل',
                          style: Theme.of(context).textTheme.bodyText1),
                    ),
                    const Text('|'),
                    TextButton(
                      onPressed: () {},
                      child: Text('تسميع',
                          style: Theme.of(context).textTheme.bodyText1),
                    )
                  ],
                ),
                color: (index % 2 == 0 && widget.status == 'حاضر')
                    ? const Color(0xFFDCFCC6)
                    : (widget.status == 'غائب')
                        ? Colors.red
                        : (widget.status == 'مأذون')
                            ? Colors.orange
                            : Colors.white,
                padding: 4,
                marginVertical: 0,
                marginVerticalBottom: 15,
                radiusBottom: 15,
                shadow: true,
              )),
          // SizedBox(height: openWidget ? 17 : 5),
        ],
      ),
      itemCount: 9,
    );
  }
}

class BuildAbsenceWidget extends StatelessWidget {
  const BuildAbsenceWidget({
    Key? key,
    required this.context,
    required this.absence,
    required this.presence,
  }) : super(key: key);

  final BuildContext context;
  final String absence;
  final String presence;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 15),
        Text(
          absence,
          style: Theme.of(context)
              .textTheme
              .headline2!
              .copyWith(color: Colors.red),
        ),
        Text(
          '   عدد الغياب ',
          style: Theme.of(context).textTheme.headline2,
        ),
        const Spacer(),
        Text(
          presence,
          style: Theme.of(context)
              .textTheme
              .headline2!
              .copyWith(color: Colors.green),
        ),
        Text(
          '  عدد الحضور ',
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(width: 10),
      ],
    );
  }
}

class BuildRichTextDayAndDate extends StatelessWidget {
  const BuildRichTextDayAndDate({
    Key? key,
    required this.context,
    required this.day,
    required this.date,
  }) : super(key: key);

  final BuildContext context;
  final String day;
  final String date;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8),
      padding: const EdgeInsets.all(10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: const Color(0xFFC8C8C8))),
      child: RichText(
        text: TextSpan(
          text: day,
          style: Theme.of(context).textTheme.bodyText1,
          children: <TextSpan>[
            TextSpan(
                text: ' |  ',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(fontSize: 25)),
            TextSpan(text: date),
          ],
        ),
      ),
    );
  }
}
