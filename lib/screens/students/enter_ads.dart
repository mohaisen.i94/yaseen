import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/landing_page.dart';
import 'package:yaseen_app/screens/students/students_screen.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/landing_second_widget.dart';
import 'package:yaseen_app/widgets/landing_widget.dart';

class EnterAds extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'إدخال الإعلانات',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              buildStudentWidget(
                  context: context,
                  name: 'عبد الله عبد الرحمن عبد الله ',
                  date: '2020/20/21'),
              buildNewsWidget(
                  context: context,
                  date: '2020/20/21',
                  title: 'دورة أحكام',
                  text:
                      'تفاصيل الخبر  تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر تفاصيل الخبر'),
              buildNameWidget(
                  context: context,
                  date: '2020/21/21',
                  name: 'عبد الله عبد الرحمن عبد الله ',
                  number: 19),
              buildImage(context: context, image: 'images/img.png'),
              CostumeContainer(
                marginHorizontal: 5,
                height: 50,
                marginVertical: 10,
                marginVerticalBottom: 5,
                color: const Color(0xFFF1F8ED),
                child: Text(
                  'حفظ',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  CostumeContainer buildImage({
    required BuildContext context,
    required image,
  }) {
    return CostumeContainer(
      height: 180,
      color: const Color(0xFFF1F8ED),
      radiusBottom: 20,
      radiusTop: 20,
      padding: 10,
      child: Row(
        children: [
          buildElevatedButton(context, () {}),
          SizedBox(width: 40),
          Expanded(
            child: Container(
                clipBehavior: Clip.antiAlias,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                )),
          ),
          SizedBox(width: 30),
          FloatingActionButton(
            onPressed: () {},
            elevation: 0,
            child: Icon(Icons.add),
            backgroundColor: Theme.of(context).primaryColor,
            mini: true,
          ),
        ],
      ),
    );
  }

  CostumeContainer buildNameWidget(
      {required BuildContext context,
      required name,
      required date,
      required number}) {
    return CostumeContainer(
      padding: 10,
      radiusBottom: 20,
      radiusTop: 20,
      color: const Color(0xFFF1F8ED),
      child: Column(
        children: [
          NameStudentWidget(
            date: date,
            name: name,
            number: number,
            isAdd: true,
            margin: 0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              buildElevatedButton(context, () {}),
              FloatingActionButton(
                onPressed: () {},
                elevation: 0,
                child: Icon(Icons.add),
                backgroundColor: Theme.of(context).primaryColor,
                mini: true,
              ),
            ],
          ),
        ],
      ),
    );
  }

  CostumeContainer buildNewsWidget(
      {required BuildContext context,
      required date,
      required title,
      required text}) {
    return CostumeContainer(
      height: 180,
      color: const Color(0xFFF1F8ED),
      radiusBottom: 20,
      radiusTop: 20,
      padding: 10,
      child: Row(
        children: [
          buildElevatedButton(context, () {}),
          SizedBox(width: 20),
          LandingSecondWidget(
            date: date,
            title: title,
            text: text,
            color: Theme.of(context).primaryColor,
            containerColor: const Color(0xFFF1F8ED),
          ),
          SizedBox(width: 20),
          FloatingActionButton(
            onPressed: () {},
            elevation: 0,
            child: Icon(Icons.add),
            backgroundColor: Theme.of(context).primaryColor,
            mini: true,
          ),
        ],
      ),
    );
  }

  CostumeContainer buildStudentWidget(
      {required BuildContext context, required date, required name}) {
    return CostumeContainer(
      padding: 10,
      height: 200,
      color: const Color(0xFFF1F8ED),
      radiusBottom: 20,
      radiusTop: 20,
      child: Row(
        children: [
          buildElevatedButton(context, () {}),
          SizedBox(width: 20),
          Expanded(
            child: LandingWidget(
                color: const Color(0xFFF1F8ED),
                smallColor: Theme.of(context).primaryColor,
                date: date,
                name: name),
          ),
          SizedBox(width: 20),
          FloatingActionButton(
            onPressed: () {},
            elevation: 0,
            child: Icon(Icons.add),
            backgroundColor: Theme.of(context).primaryColor,
          )
        ],
      ),
    );
  }

  Widget buildElevatedButton(BuildContext context, VoidCallback function) {
    return ElevatedButton(
      onPressed: function,
      child: Text(
        'موافق',
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: Colors.white),
      ),
      style: ElevatedButton.styleFrom(primary: Theme.of(context).primaryColor),
    );
  }
}
