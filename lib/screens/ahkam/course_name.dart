import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/ahkam/add_result_test.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_course.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_recorder_std.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_std_data.dart';
import 'package:yaseen_app/screens/ahkam/course_recorder.dart';
import 'package:yaseen_app/screens/circles/circle_children.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CourseName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AhkamCourse(),
              )),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'اسم الدورة',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: Column(
          children: [
            BuildAbsenceWidget(context: context, absence: '10', presence: '25'),
            Row(
              children: [
                CostumeContainer(
                  function: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CourseRecorder(),
                      )),
                  child: Text(
                    'السجل',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  width: 110,
                  height: 55,
                  color: const Color(0xFFDCFCC6),
                ),
                const SizedBox(width: 5),
                Expanded(
                  child: BuildRichTextDayAndDate(
                      context: context, day: 'السبت', date: '24/12/20'),
                )
              ],
            ),
            Row(
              children: [
                const Expanded(
                  child: BuildTextField(
                    text: 'أدخل النص هنا ',
                    height: 55,
                  ),
                ),
                const SizedBox(width: 5),
                CostumeContainer(
                  child: Text(
                    'عنوان الدرس',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  width: 140,
                  height: 55,
                  color: const Color(0xFFDCFCC6),
                ),
              ],
            ),
            const SizedBox(height: 5),
            Expanded(
              child: BuildRowListView(
                function: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AhkamStdData(),
                    )),
                visible: false,
                status: 'حاضر',
                name: 'اسم الطالب',
                visibleStar: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Row(
                children: [
                  Expanded(
                    child: CostumeContainer(
                      function: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AddResultTest(),
                          )),
                      child: Text(
                        'إضافة نتائج اختبار',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white),
                      ),
                      color: Theme.of(context).primaryColor,
                      padding: 5,
                      height: 55,
                    ),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: CostumeContainer(
                      function: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AhkamStdData(),
                          )),
                      child: Text(
                        'إضافة طالب جديد',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white),
                      ),
                      color: Theme.of(context).primaryColor,
                      padding: 5,
                      height: 55,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
