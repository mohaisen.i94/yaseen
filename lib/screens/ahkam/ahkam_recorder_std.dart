import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/ahkam/course_name.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class AhkamRecorderStd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CourseName(),
              )),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'سجل الطالب - أحكام',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Column(
          children: [
            Row(
              children: [
                const Expanded(
                    child: CostumeContainer(
                  child: Text('اسم الطالب'),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                )),
                CostumeImage(
                  radius: 90,
                  marginVir: 2,
                )
              ],
            ),
            const SizedBox(height: 15),
            Expanded(
                child: CostumeContainer(
              child: Table(
                textDirection: TextDirection.rtl,
                columnWidths: const {
                  0: FractionColumnWidth(.33),
                  1: FractionColumnWidth(.33),
                  2: FractionColumnWidth(.33),
                },
                defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
                children: [
                  TableRow(children: [
                    buildCostumeContainer(context, 'اليوم', Colors.orange),
                    buildCostumeContainer(context, 'الدرس', Colors.orange),
                    buildCostumeContainer(context, 'الحضور', Colors.orange),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, 'اليوم', Colors.orange[100]),
                    buildCostumeContainer(context, 'الدرس', Colors.orange[100]),
                    buildCostumeContainer(
                        context, 'الحضور', Colors.orange[100]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                    buildCostumeContainer(context, '', Colors.orange[100]),
                  ]),
                  TableRow(children: [
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                    buildCostumeContainer(context, '', Colors.orange[50]),
                  ]),
                ],
              ),
              color: Colors.white,
              alignment: Alignment.topCenter,
              padding: 15,
            ))
          ],
        ),
      ),
    );
  }

  CostumeContainer buildCostumeContainer(BuildContext context, text, color) {
    return CostumeContainer(
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: Colors.black),
      ),
      color: color,
      marginVerticalBottom: 0,
      marginVertical: 0,
      shadow: false,
      height: 40,
      borderColor: Colors.white,
    );
  }
}
