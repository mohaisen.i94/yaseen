import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class UsersScreen extends StatelessWidget {
  List list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'المستخدمون',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            DropDownMenuCostume(
                ctx: context,
                text: 'الإدارة / المحفظون / الطلاب ',
                height: 50,
                list: list,
                radius: 8),
            DropDownMenuCostume(
              ctx: context,
              text: 'الإدارة / المحفظون / الطلاب ',
              height: 50,
              list: list,
              radius: 8,
            ),
            BuildTextField(text: 'للبحث أدخل النص هنا'),
            CostumeContainer(
              height: 470,
              padding: 5,
              radiusTop: 8,
              radiusBottom: 8,
              color: Colors.white,
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(width: 20),
                      Text(
                        'كلمة المرور',
                        style: TextStyle(color: Colors.red, fontSize: 18),
                      ),
                      Spacer(),
                      Text(
                        'الحساب',
                        style: TextStyle(color: Colors.red, fontSize: 18),
                      ),
                      Spacer(),
                      Text(
                        'الاسم كاملا',
                        style: TextStyle(color: Colors.red, fontSize: 18),
                      ),
                      SizedBox(width: 20),
                    ],
                  ),
                  Divider(
                    color: Colors.grey,
                    thickness: .4,
                    endIndent: 10,
                    indent: 10,
                    height: 10,
                  ),
                  SizedBox(height: 10),
                  Expanded(child: listViewUsers()),
                ],
              ),
            ),
            CostumeContainer(
              height: 50,
              marginVertical: 40,
              color: Theme.of(context).primaryColor,
              child: Text(
                'موافق',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget listViewUsers() {
    return ListView.builder(
      itemBuilder: (context, index) => Container(
        margin: EdgeInsets.zero,
        height: 45,
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(width: 30),
                Text(
                  '505511221',
                ),
                Spacer(),
                Text('محمد '),
                Spacer(),
                Text('محمد أحمد علي'),
                SizedBox(width: 15),
              ],
            ),
            Divider(
              color: Colors.grey,
              thickness: .4,
              endIndent: 10,
              indent: 10,
              height: 2,
            ),
          ],
        ),
      ),
    );
  }
}
