import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/management.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/screens/management/widgets/costume_user_and_passowrd.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

import '../../data.dart';

class NewTeacherScreen extends StatelessWidget {
  var dropDownChoose;
  List list = ['حلقة 1', 'حلقة 2', 'حلقة 3'];
  List<DataApp> data = [
    DataApp(mainText: 'الاسم ثلاثي', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'تاريخ الميلاد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'العـمــر', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'السـكــن', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الجــــوال', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'دورات أحكام', subText: 'أدخل النص هنا'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const ManagementScreen(),
                ));
          },
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'محفظ جديد',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: Column(
          children: [
            CostumeImage(
              radius: 140,
              marginVir: 20,
            ),
            CostumeListView(data: data),
            Row(
              children: [
                Expanded(
                    child: DropDownMenuCostume(
                        ctx: context,
                        text: 'محفظ (اختيار حلقة / إداري',
                        height: 50,
                        list: list)),
                CostumeContainer(
                    width: 140,
                    height: 55,
                    child: Text(
                      'المــهــمــة',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                    color: Theme.of(context).primaryColor)
              ],
            ),
            CostumeUserAndPassowrd(
              passwordFunction: () {},
              userFunction: () {},
            ),
            const SizedBox(height: 10),
            CostumeContainer(
              child: Text('موافق',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
              color: Theme.of(context).primaryColor,
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
