import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class TeacherRecorder extends StatelessWidget {
  List list = ['المحفظ', 'الطالب'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'سجل المحفظ',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: DropDownMenuCostume(
                    ctx: context,
                    text: 'المحفظ',
                    height: 44,
                    list: list,
                    margin: 0,
                  ),
                ),
                Container(
                    width: 77,
                    height: 77,
                    margin: const EdgeInsets.only(bottom: 10),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(5)),
                    clipBehavior: Clip.antiAlias,
                    child: Image.asset(
                      'images/img.png',
                      fit: BoxFit.cover,
                    ))
              ],
            ),
            CostumeContainer(
              child: Text(
                'سجل الدوام',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
              color: Theme.of(context).primaryColor,
              padding: 0,
              height: 40,
            ),
            Row(
              children: const [
                CostumeContainer(
                  child: Text('من - إلى'),
                  color: Colors.white,
                  width: 190,
                  height: 41,
                  padding: 0,
                ),
                Expanded(
                    child: CostumeContainer(
                  child: Text('أسبوعي'),
                  color: Colors.white,
                  marginHorizontal: 5,
                  padding: 0,
                  height: 41,
                )),
                Expanded(
                    child: CostumeContainer(
                  child: Text('شهري'),
                  color: Colors.white,
                  padding: 0,
                  height: 41,
                )),
              ],
            ),
            CostumeContainer(
              child: Text(''),
              color: Colors.white,
              padding: 15,
            )
          ],
        ),
      ),
    );
  }
}
