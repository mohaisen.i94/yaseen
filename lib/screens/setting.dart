import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/bn_gradiant.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BNGradiant(
        child: Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات الطالب',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            alignment: Alignment.centerRight,
            height: 55,
            child: Text('إعدادات التطبيق      ',
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Colors.black)),
          ),
          CostumeContainer(
            padding: 7,
            shadow: false,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'تسجيل خروج',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      SizedBox(width: 10),
                      Icon(
                        Icons.exit_to_app,
                        color: Colors.grey,
                      )
                    ],
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 20,
                    endIndent: 5,
                    indent: 5,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'مشاركة التطبيق',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      SizedBox(width: 10),
                      Icon(
                        Icons.share,
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            height: 55,
            child: Text('تواصل معنا      ',
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Colors.black)),
          ),
          CostumeContainer(
            padding: 7,
            shadow: false,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'هاتف',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      SizedBox(width: 20),
                      Icon(
                        Icons.phone,
                      )
                    ],
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 20,
                    endIndent: 5,
                    indent: 5,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'واتس',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      SizedBox(width: 20),
                      Image.asset('images/whatsapp.png'),
                    ],
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 20,
                    endIndent: 5,
                    indent: 5,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'فيس بوك',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      SizedBox(width: 10),
                      Image.asset('images/face.png'),
                    ],
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 20,
                    endIndent: 5,
                    indent: 5,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'تويتر',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.black),
                      ),
                      SizedBox(width: 10),
                      Image.asset('images/twitter.png'),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            height: 55,
            child: Text('عن المركز      ',
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Colors.black)),
          ),
          Expanded(
              child: CostumeContainer(
            color: Colors.white,
            child: Text(
              '''هو عبارة عن نظام كامل لإدارة مركز التحفيظ, ويقدم خصائص 
تجعل المركز يعتمد عليه بشكل كامل في كل العمليات, حيث 
يمكن تسجيل حضور للطلاب يوميا من حساب المحفظ أو من 
حساب الإدارة, ويتيح لحساب ولي الأمر متابعة الحضور 
والغياب لابنه ويمكنه إرسال شكوى أو مقترح لإدارة المركز تتم 
متابعته من خلال حساب المدير, ويقدم التطبيق خدمة 
التواصل بين ولي الأمر المحفظ. ''',
              textAlign: TextAlign.right,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: Colors.black),
            ),
          )),
        ],
      ),
    ));
  }
}
