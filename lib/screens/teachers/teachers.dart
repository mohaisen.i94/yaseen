import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/new_teacher.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class TeacherScreen extends StatelessWidget {
  const TeacherScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'المحفظين',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          children: [
            Row(
              children: [
                CostumeContainer(
                  function: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => NewTeacherScreen(),
                      )),
                  child: Text(
                    'جديد',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  color: Colors.white,
                  height: 55,
                  width: 77,
                  marginHorizontal: 2,
                  shadow: false,
                ),
                Expanded(
                    child: CostumeContainer(
                  child: Text(
                    'الحضور والغياب',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  color: Colors.white,
                  height: 55,
                  marginHorizontal: 2,
                  shadow: false,
                ))
              ],
            ),
            const SizedBox(height: 8),
            const BuildTextField(
              text: 'للبحث ادخل النص هنا',
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 8),
            Expanded(
                child: ListView.builder(
              itemBuilder: (context, index) => CostumeContainer(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('حلقة الثانوية'),
                      Spacer(),
                      Text(' عبد الله عبد الرحمن عبد الله'),
                      Container(
                          margin: const EdgeInsets.only(left: 30),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5)),
                          width: 40,
                          height: 40,
                          clipBehavior: Clip.antiAlias,
                          child: Image.asset(
                            'images/img.png',
                            fit: BoxFit.cover,
                          )),
                    ],
                  ),
                  height: 60,
                  padding: 10,
                  shadow: false,
                  color: (index % 2 == 0)
                      ? Colors.white
                      : const Color(0xFFDCFCC6)),
              itemCount: 10,
            )),
            buildCostumeButton(context, 30),
          ],
        ),
      ),
    );
  }

  CostumeContainer buildCostumeButton(BuildContext context, int number) {
    return CostumeContainer(
      child: RichText(
        text: TextSpan(
            style: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(color: Colors.white),
            text: 'عدد المحفظين',
            children: [
              const TextSpan(text: '     '),
              TextSpan(text: '$number'),
            ]),
      ),
      color: Colors.green,
      padding: 15,
      marginHorizontal: 50,
      marginVertical: 15,
      radiusBottom: 15,
      radiusTop: 15,
    );
  }
}
