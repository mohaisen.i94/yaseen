import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class MessageBubble extends StatelessWidget {
  final bool isMe;
  final String message;

  MessageBubble({required this.isMe, required this.message});
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isMe ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        child: Text(
          message,
          style: isMe
              ? Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: Colors.white, fontSize: 15)
              : Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: Colors.black, fontSize: 15),
        ),
        decoration: BoxDecoration(
          color: isMe ? Theme.of(context).primaryColor : Colors.grey[400],
          borderRadius: BorderRadius.only(
              topLeft: isMe ? Radius.circular(30) : Radius.circular(10),
              bottomLeft: isMe ? Radius.circular(30) : Radius.circular(0),
              topRight: isMe ? Radius.circular(10) : Radius.circular(30),
              bottomRight: isMe ? Radius.circular(0) : Radius.circular(30)),
        ),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        margin: isMe
            ? const EdgeInsets.only(right: 15, left: 50, bottom: 5)
            : const EdgeInsets.only(right: 50, left: 15, bottom: 5),
      ),
    );
  }
}
