import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class MotivationScreen extends StatelessWidget {
  List list = ['dtthg', 'dhghg', 'jbjhfbj'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'تقرير الطلاب / حلقة',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
          children: [
            CostumeContainer(
              child: Column(
                children: [
                  const SizedBox(height: 15),
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الحلقات / المحفظين ',
                    height: 50,
                    list: list,
                    radius: 5,
                  ),
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الجميع / اختيار اسم',
                    height: 50,
                    list: list,
                    radius: 5,
                  ),
                  CostumeContainer(
                    child: Text(
                      'موافق',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: CupertinoColors.white),
                    ),
                    color: Theme.of(context).primaryColor,
                    marginVertical: 10,
                    height: 50,
                    padding: 0,
                    radiusBottom: 6,
                    radiusTop: 6,
                    marginVerticalBottom: 10,
                  ),
                ],
              ),
              color: const Color(0xFFF5F5F5),
              radiusBottom: 8,
              radiusTop: 8,
              padding: 10,
              borderColor: Colors.white,
              borderWidth: 2,
            ),
            // CostumeContainer(child: buildTable(), color: Colors.white)
          ],
        ),
      ),
    );
  }

  Widget buildTable() {
    return Column();
  }
}
