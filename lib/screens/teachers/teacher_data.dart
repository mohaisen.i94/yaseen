import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/screens/management/widgets/teacher_and_std_name.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

import '../../data.dart';

class TeacherData extends StatelessWidget {
  List<DataApp> data = [
    DataApp(mainText: 'الاسم ثلاثي', subText: 'أدخل النص هنا'),
    DataApp(mainText: ' تاريخ الميلاد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'العمر', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'السكن', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الجوال', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'دزرات أحكام', subText: 'أدخل النص هنا'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات المحفظين',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child:
            SingleChildScrollView(child: buildTeacherDataBody(context, data)),
      ),
    );
  }

  Widget buildTeacherDataBody(BuildContext context, List data) {
    return Column(children: [
      TeacherAndStdName(
        text: 'الحلقة',
        name: 'اسم المحفظ',
      ),
      Row(
        children: [
          CostumeContainer(
              child: Text(
                'حذف',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
              height: 60,
              width: 90,
              marginHorizontal: 2,
              color: Color(0xFFBB2D2D)),
          Expanded(
            child: CostumeContainer(
                marginHorizontal: 2,
                height: 60,
                child: Text(
                  'سجل الدوام',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                color: Colors.white),
          ),
          Expanded(
            child: CostumeContainer(
                marginHorizontal: 2,
                height: 60,
                child: Text(
                  'إرسال رسالة',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                color: Colors.white),
          )
        ],
      ),
      const SizedBox(height: 15),
      CostumeListView(data: data),
      const SizedBox(height: 20),
      Row(
        children: [
          Expanded(
            child: CostumeContainer(
              height: 50,
              marginVerticalBottom: 0,
              color: Theme.of(context).primaryColor,
              child: Text('ملاحظات',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
            ),
          ),
        ],
      ),
      BuildTextField(
        text: 'ترسل الملاحظات للمحفظ عبر البريد',
        textAlign: TextAlign.center,
        borderColor: Theme.of(context).primaryColor,
      ),
      const SizedBox(height: 20),
    ]);
  }
}
