import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class StudentTest2 extends StatelessWidget {
  List list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'اختبارات الطالب',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            CostumeContainer(
              child: Column(
                children: [
                  Text(
                    'فلترة حسب',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'بحث',
                    height: 45,
                    list: list,
                    radius: 5,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: CostumeContainer(
                      child: Text('موافق',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white)),
                      color: Theme.of(context).primaryColor,
                      width: 100,
                      height: 45,
                      marginVertical: 20,
                      marginHorizontal: 3,
                      radiusTop: 5,
                      radiusBottom: 5,
                    ),
                  )
                ],
              ),
              padding: 20,
              color: const Color(0xFFF5F5F5),
              radiusTop: 5,
              radiusBottom: 5,
              borderColor: Colors.white,
              borderWidth: 1,
              marginVerticalBottom: 15,
            ),
          ],
        ),
      ),
    );
  }
}
