import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class LandingSecondWidget extends StatelessWidget {
  const LandingSecondWidget({
    Key? key,
    required this.date,
    required this.title,
    required this.text,
    this.color = Colors.orange,
    this.containerColor = Colors.white,
  }) : super(key: key);

  final date;
  final title;
  final text;
  final color;
  final containerColor;

  @override
  Widget build(BuildContext context) {
    return CostumeContainer(
      width: 250,
      marginHorizontal: 5,
      color: containerColor,
      radiusBottom: 7,
      radiusTop: 7,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              padding: const EdgeInsets.all(5),
              height: 30,
              child: Text(
                date,
              ),
              decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadiusDirectional.only(
                      topStart: Radius.circular(7),
                      bottomEnd: Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(.16),
                      blurRadius: 6,
                      offset: Offset(0, 3),
                    )
                  ]),
            ),
          ),
          Positioned(
              child: Padding(
            padding: const EdgeInsets.only(right: 15, top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              textDirection: TextDirection.ltr,
              children: [
                Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  text,
                  textAlign: TextAlign.end,
                )
              ],
            ),
          )),
          Positioned(
            right: 0,
            top: 0,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                color: color,
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(7),
                    bottomRight: Radius.circular(7)),
              ),
              width: (7),
            ),
          )
        ],
      ),
    );
  }
}
