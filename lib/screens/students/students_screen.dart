import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class StudentScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'الحفظة',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Stack(
        children: [
          CostumeContainer(
            marginVerticalBottom: 20,
            marginVertical: 15,
            marginHorizontal: 15,
            padding: 5,
            color: Colors.white,
            radiusBottom: 25,
            radiusTop: 25,
            borderWidth: 1.5,
            child: ListView.builder(
              padding: EdgeInsets.all(10),
              itemBuilder: (context, index) => const NameStudentWidget(
                name: 'عبد الله عبد الرحمن عبد الله',
                number: 19,
                date: '2020.12.22',
              ),
              itemCount: 10,
            ),
          ),
          Positioned(
            bottom: 3,
            right: 30,
            child: FloatingActionButton(
              onPressed: () {},
              child: Icon(Icons.add),
              backgroundColor: Theme.of(context).primaryColor,
              mini: true,
            ),
          )
        ],
      ),
    );
  }
}

class NameStudentWidget extends StatelessWidget {
  const NameStudentWidget({
    Key? key,
    required this.date,
    required this.name,
    required this.number,
    this.isAdd = false,
    this.margin = 15,
  }) : super(key: key);
  final date;
  final name;
  final number;
  final bool isAdd;
  final double margin;

  @override
  Widget build(BuildContext context) {
    return CostumeContainer(
      height: 120,
      marginVerticalBottom: margin,
      color: Color(0xFFF1F8ED),
      radiusTop: 25,
      radiusBottom: 25,
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  color: Theme.of(context).primaryColor),
              child: Text(
                '$number',
                style: TextStyle(color: Colors.white),
              ),
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            ),
          ),
          !isAdd
              ? Positioned(
                  left: 0,
                  bottom: 0,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          topRight: Radius.circular(5),
                          bottomRight: Radius.circular(5),
                        ),
                        color: Theme.of(context).primaryColor),
                    child: Text(
                      '<<   المزيد ',
                      style: TextStyle(color: Colors.white),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                  ),
                )
              : Center(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'الحافظ لكتاب الله',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    name,
                    style: TextStyle(
                        color: Color(0xFF563319),
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                  RichText(
                      text: TextSpan(
                          text: '$date',
                          style: const TextStyle(
                              color: Color(0xFF0C5263),
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                          children: const [
                        TextSpan(text: '  '),
                        TextSpan(text: 'تاريخ إتمام الحفظ')
                      ]))
                ],
              ),
              SizedBox(width: 20),
              CircleAvatar(
                radius: 40,
                backgroundColor: Theme.of(context).primaryColor,
              ),
              SizedBox(width: 10),
            ],
          ),
        ],
      ),
    );
  }
}
