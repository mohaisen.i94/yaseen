import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class AddPlanManually extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          ' إضافة خطة يدوي',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: CostumeContainer(
                  child: Text('اسم الطالب',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                )),
                CostumeImage(
                  radius: 90,
                  marginVir: 2,
                )
              ],
            ),
            CostumeContainer(
              height: 50,
              marginVerticalBottom: 15,
              marginVertical: 20,
              color: Colors.white,
              child: Text('إدخال الخطة',
                  style: Theme.of(context).textTheme.bodyText1),
            ),
            CostumeContainer(color: Colors.white),
          ],
        ),
      ),
    );
  }
}
