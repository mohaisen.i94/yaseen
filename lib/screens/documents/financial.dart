import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class FinancialReport extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'تقرير الطلاب / حلقة',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
        child: Column(
          children: [
            CostumeContainer(
              child: Text(
                'فترة التقرير',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
              color: Theme.of(context).primaryColor,
              marginVertical: 3,
              height: 50,
              padding: 0,
              marginVerticalBottom: 2,
            ),
            Row(
              children: [
                CostumeContainer(
                  function: () {},
                  child: Text('من - إلى',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  width: 190,
                  height: 50,
                  padding: 0,
                ),
                Expanded(
                    child: CostumeContainer(
                  function: () {},
                  child: Text('أسبوعي',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  marginHorizontal: 5,
                  padding: 0,
                  height: 50,
                )),
                Expanded(
                    child: CostumeContainer(
                  function: () {},
                  child: Text('شهري',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  padding: 0,
                  height: 50,
                )),
              ],
            ),
            Row(
              children: [
                CostumeContainer(
                  child:
                      Text('1000', style: Theme.of(context).textTheme.bodyText1)
                  /* TextField(
                    decoration:
                        InputDecoration(enabledBorder: InputBorder.none),
                  )*/
                  ,
                  color: Colors.white,
                  width: 190,
                  height: 50,
                  padding: 0,
                ),
                Expanded(
                    child: CostumeContainer(
                  child: Text(
                    'قيمة الصرف',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white),
                  ),
                  color: Theme.of(context).primaryColor,
                  padding: 0,
                  height: 50,
                )),
              ],
            ),
            CostumeContainer(child: buildTable(), color: Colors.white)
          ],
        ),
      ),
    );
  }

  Widget buildTable() {
    return Column();
  }
}
