// ignore_for_file: use_key_in_widget_constructors

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class PresentScreen extends StatefulWidget {
  @override
  State<PresentScreen> createState() => _PresentScreenState();
}

class _PresentScreenState extends State<PresentScreen> {
  final List<Color> gradiantColors = [Colors.red, Colors.transparent];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'أعداد الحضور',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          children: [
            CostumeContainer(
              child: Text(
                'الطــلاب',
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Colors.black, fontSize: 25),
              ),
              color: const Color(0xFFC8C7FD),
              radiusTop: 10,
              height: 50,
              marginVerticalBottom: 2,
            ),
            tableWidget(context, 'السبت', '20/21/20'),
            const SizedBox(height: 15),
            CostumeContainer(
              child: Text(
                'المحفظين',
                style: Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: Colors.black, fontSize: 25),
              ),
              color: const Color(0xFF9DDCFF),
              radiusTop: 10,
              height: 50,
              marginVerticalBottom: 2,
            ),
            tableWidget(context, 'السبت', '20/21/20'),
            const SizedBox(height: 15),
            // chartWidget,
            Expanded(
              child: Container(
                padding: EdgeInsets.all(20),
                color: Colors.white,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Statistics',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal)),
                        ElevatedButton.icon(
                          onPressed: () {},
                          label: Text(
                            'Last 6 months',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal),
                          ),
                          icon: const Icon(
                            Icons.keyboard_arrow_down_sharp,
                            color: Colors.grey,
                          ),
                          style: ElevatedButton.styleFrom(
                              elevation: 5,
                              shadowColor: Colors.grey,
                              shape: RoundedRectangleBorder(
                                  side: const BorderSide(
                                      color: Colors.grey, width: 1.5),
                                  borderRadius: BorderRadius.circular(10)),
                              primary: Colors.white),
                        )
                      ],
                    ),
                    const SizedBox(height: 15),
                    SizedBox(
                      height: 250,
                      width: 395,
                      child: LineChart(
                        LineChartData(
                            maxX: 5,
                            minX: 0,
                            maxY: 5,
                            minY: 0,
                            titlesData: FlTitlesData(
                              show: true,
                              topTitles: SideTitles(
                                showTitles: false,
                              ),
                              rightTitles: SideTitles(
                                showTitles: false,
                              ),
                              leftTitles: SideTitles(
                                  showTitles: true,
                                  reservedSize: 35,
                                  margin: 10,
                                  getTextStyles: (context, value) {
                                    TextStyle(color: Colors.red, fontSize: 10);
                                  },
                                  getTitles: (value) {
                                    switch (value.toInt()) {
                                      case 1:
                                        return '\$5k';
                                      case 2:
                                        return '\$10';
                                      case 3:
                                        return '\$15k';
                                      case 4:
                                        return '\$20k';
                                      case 5:
                                        return '\$25k';
                                    }
                                    return '';
                                  }),
                              bottomTitles: SideTitles(
                                  showTitles: true,
                                  margin: 10,
                                  reservedSize: 30,
                                  getTitles: (value) {
                                    switch (value.toInt()) {
                                      case 0:
                                        return 'Jan';
                                      case 1:
                                        return 'Feb';
                                      case 2:
                                        return 'Mar';
                                      case 3:
                                        return 'Apr';
                                      case 4:
                                        return 'May';
                                      case 5:
                                        return 'Jun';
                                    }
                                    return '';
                                  }),
                            ),
                            gridData: FlGridData(
                              show: true,
                              getDrawingVerticalLine: (value) {
                                return FlLine(
                                    color: Colors.grey.shade100,
                                    strokeWidth: 1);
                              },
                              getDrawingHorizontalLine: (value) {
                                return FlLine(
                                    color: Colors.grey.shade200,
                                    strokeWidth: 1);
                              },
                            ),
                            borderData: FlBorderData(
                                show: false,
                                border:
                                    Border.all(color: Colors.red, width: 2)),
                            lineBarsData: [
                              LineChartBarData(
                                  spots: const [
                                    FlSpot(0, 3),
                                    FlSpot(1, 2),
                                    FlSpot(3, 3),
                                    FlSpot(5, 2),
                                  ],
                                  barWidth: 5,
                                  // dotData: FlDotData(show: false ),
                                  colors: [Colors.lightBlue[200]!],
                                  isCurved: true,
                                  belowBarData: BarAreaData(
                                      show: true,
                                      // gradientFrom: Offset(0, 5),
                                      colors: [
                                        Colors.lightBlue[200]!,
                                        Colors.white
                                      ]
                                          .map((e) => e.withOpacity(.26))
                                          .toList())),
                              LineChartBarData(
                                  spots: const [
                                    FlSpot(0, 2),
                                    FlSpot(1, 1),
                                    FlSpot(3, 3),
                                    FlSpot(5, 1),
                                  ],
                                  barWidth: 5,
                                  shadow: Shadow(
                                      color: Colors.deepPurpleAccent[100]!,
                                      blurRadius: 20,
                                      offset: Offset(0, 3)),
                                  // dotData: FlDotData(show: false),
                                  colors: [Colors.deepPurpleAccent[100]!],
                                  isCurved: true,
                                  belowBarData: BarAreaData(
                                      show: true,
                                      colors: [
                                        Colors.deepPurpleAccent[100]!,
                                        Colors.white
                                      ]
                                          .map((e) => e.withOpacity(0.26))
                                          .toList())),
                            ]),
                      ),
                    ),
                    Row(
                      children: [
                        indicator(Colors.deepPurpleAccent[100]!),
                        const SizedBox(width: 10),
                        const Text('الطلاب'),
                        const SizedBox(width: 50),
                        indicator(Colors.lightBlue[200]!),
                        const SizedBox(width: 10),
                        const Text('المحفظين'),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget indicator(Color color) {
    return Container(
      height: 15,
      width: 40,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(8)),
    );
  }

  Widget tableWidget(BuildContext context, String day, String date) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  buildCostumeContainerTable(
                      child: Column(
                        children: [Text(day), Text(date)],
                      ),
                      height: 55),
                  buildCostumeContainerTable(
                      child: Text(date), color: Colors.amber[200]!),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  buildCostumeContainerTable(
                      child: Column(
                        children: [Text(day), Text(date)],
                      ),
                      height: 55),
                  buildCostumeContainerTable(
                      child: Text(date), color: Colors.amber[200]!),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  buildCostumeContainerTable(
                      child: Column(
                        children: [Text(day), Text(date)],
                      ),
                      height: 55),
                  buildCostumeContainerTable(
                      child: Text(date), color: Colors.amber[200]!),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  buildCostumeContainerTable(
                      child: Column(
                        children: [Text(day), Text(date)],
                      ),
                      height: 55),
                  buildCostumeContainerTable(
                      child: Text(date), color: Colors.amber[200]!),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  buildCostumeContainerTable(
                      child: const Text('اليوم'), height: 55),
                  buildCostumeContainerTable(child: const Text('العدد')),
                ],
              ),
            ),
          ],
        ),
        Container(color: Colors.white, height: 8),
      ],
    );
  }

  Widget buildCostumeContainerTable({
    required Widget child,
    Color color = Colors.orange,
    double height = 40,
  }) {
    return CostumeContainer(
      child: child,
      color: color,
      borderColor: Colors.black,
      height: height,
      marginVertical: 0,
      marginVerticalBottom: 0,
      padding: 0,
      shadow: false,
      borderWidth: .5,
    );
  }
}
