import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class TestScreen extends StatelessWidget {
  var dropDownChoose;
  List list = ['اسم الطالب', 'الحلقة', 'الجزء', 'التاريخ من إلى'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'اختبارات الطالب',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            CostumeContainer(
              child: Column(
                children: [
                  Text(
                    'فلترة حسب',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'بحث',
                    height: 45,
                    list: list,
                    dropDownChoose: dropDownChoose,
                    radius: 5,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: CostumeContainer(
                      child: Text('موافق',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white)),
                      color: Theme.of(context).primaryColor,
                      width: 100,
                      height: 45,
                      marginVertical: 20,
                      marginHorizontal: 3,
                      radiusTop: 5,
                      radiusBottom: 5,
                    ),
                  )
                ],
              ),
              padding: 20,
              color: const Color(0xFFF5F5F5),
              radiusTop: 5,
              radiusBottom: 5,
              borderColor: Colors.white,
              borderWidth: 1,
              marginVerticalBottom: 15,
            ),
            Expanded(
              child: CostumeContainer(
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          buildCostumeContainerTable(
                              child: const Text('التكريم'),
                              color: Colors.amber,
                              marginVerticalBottom: 3),
                          Expanded(
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) =>
                                  buildCostumeContainerTable(
                                child: const Text(''),
                                color: (index % 2 == 0)
                                    ? Colors.orange[50]!
                                    : Colors.orange[100]!,
                              ),
                              itemCount: 13,
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          buildCostumeContainerTable(
                              child: const Text('التقدير'),
                              color: Colors.amber,
                              marginVerticalBottom: 3),
                          Expanded(
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) =>
                                  buildCostumeContainerTable(
                                child: const Text(''),
                                color: (index % 2 == 0)
                                    ? Colors.orange[50]!
                                    : Colors.orange[100]!,
                              ),
                              itemCount: 13,
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          buildCostumeContainerTable(
                              child: const Text('الجزء'),
                              color: Colors.amber,
                              marginVerticalBottom: 3),
                          Expanded(
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) =>
                                  buildCostumeContainerTable(
                                child: const Text(''),
                                color: (index % 2 == 0)
                                    ? Colors.orange[50]!
                                    : Colors.orange[100]!,
                              ),
                              itemCount: 13,
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          buildCostumeContainerTable(
                              child: const Text('اليوم'),
                              color: Colors.amber,
                              marginVerticalBottom: 3),
                          Expanded(
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) =>
                                  buildCostumeContainerTable(
                                child: const Text(''),
                                color: (index % 2 == 0)
                                    ? Colors.orange[50]!
                                    : Colors.orange[100]!,
                              ),
                              itemCount: 13,
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          buildCostumeContainerTable(
                              child: const Text('الاسم'),
                              color: Colors.amber,
                              marginVerticalBottom: 3),
                          Expanded(
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) =>
                                  buildCostumeContainerTable(
                                child: const Text(''),
                                color: (index % 2 == 0)
                                    ? Colors.orange[50]!
                                    : Colors.orange[100]!,
                              ),
                              itemCount: 13,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                color: Colors.white,
                padding: 3,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildCostumeContainerTable(
      {required Widget child,
      Color color = Colors.amber,
      double height = 35,
      double marginVerticalBottom = 0}) {
    return CostumeContainer(
      child: child,
      color: color,
      borderColor: Colors.white,
      height: height,
      marginVertical: 0,
      marginVerticalBottom: marginVerticalBottom,
      padding: 0,
      shadow: false,
    );
  }
}
