// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/screens/management/widgets/costume_user_and_passowrd.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

// ignore: must_be_immutable
class NewStudentScreen extends StatelessWidget {
  List<DataApp> data = [
    DataApp(mainText: 'الاسم ثلاثي', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'تاريخ الميلاد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'العـمــر', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الـصــف', subText: 'تلقائي'),
    DataApp(mainText: 'السـكــن', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الجــــوال', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'تاريخ التسجيل', subText: 'تلقائي'),
    DataApp(mainText: 'دورات أحكام', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الحلقة', subText: 'اختيار من الحلقات المدرجة')
  ];
  List<String> dropDownList = ['حلقة 1', 'حلقة 2', 'حلقة 3'];
  var dropDownChoose;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'طالب جديد',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: SingleChildScrollView(
          child: Column(
            children: [
              CostumeImage(
                radius: 140,
                marginVir: 10,
              ),
              CostumeListView(data: data),
              CostumeUserAndPassowrd(
                passwordFunction: () {},
                userFunction: () {},
              ),
              const SizedBox(height: 10),
              CostumeContainer(
                child: Text('موافق',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white)),
                color: Theme.of(context).primaryColor,
                height: 50,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
