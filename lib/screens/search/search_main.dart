import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class SearchMainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'البحث',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                CostumeContainer(
                  child: Text('ابحث',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white)),
                  color: Theme.of(context).primaryColor,
                  width: 90,
                  height: 50,
                ),
                const Expanded(
                  child: BuildTextField(text: 'أدخل النص هنا'),
                )
              ],
            ),
            const SizedBox(height: 20),
            CostumeContainer(
              child: Text('النتيجة',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
              color: Theme.of(context).primaryColor,
              padding: 0,
              height: 50,
              marginVerticalBottom: 0,
            ),
            Expanded(
              child: CostumeContainer(
                child: Text('بحث في جميع البيانات',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.black)),
                color: Colors.white,
                borderColor: Theme.of(context).primaryColor,
                marginVertical: 0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
