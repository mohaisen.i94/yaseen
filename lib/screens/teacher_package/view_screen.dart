import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/circles/circle_children.dart';
import 'package:yaseen_app/screens/documents/document.dart';
import 'package:yaseen_app/screens/documents/tests.dart';
import 'package:yaseen_app/screens/search/chat/chat.dart';
import 'package:yaseen_app/screens/students/motivation_student.dart';
import 'package:yaseen_app/screens/students/sms_with_phone.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class ViewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List data = [
      DataApp(
          mainText: 'طلاب الحلقة',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleChildren(),
              ))),
      DataApp(
          mainText: 'البريد',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Chat1(),
              ))),
      DataApp(
          mainText: 'تقارير الطلاب',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DocumentScreen(),
              ))),
      DataApp(
          mainText: 'إدخال الحوافز',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MotivationStudent(),
              ))),
      DataApp(
          mainText: ' اختبارات الطلاب ',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TestScreen(),
              ))),
      DataApp(
          mainText: 'التواصل عبر الجوال',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SMSWithPhone(),
              ))),
      DataApp(mainText: '......', color: Colors.white, function: () {}),
      DataApp(mainText: '......', color: Colors.white, function: () {}),
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'واجهة المحفظ',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            const SizedBox(height: 8),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, index) => CostumeContainer(
                padding: 0,
                height: 70,
                marginHorizontal: 15,
                child: Text(data[index].mainText,
                    style: Theme.of(context).textTheme.bodyText1),
                color: data[index].color,
                function: data[index].function,
              ),
            )
          ],
        ),
      ),
    );
  }
}
