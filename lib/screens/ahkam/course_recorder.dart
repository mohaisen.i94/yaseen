import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/ahkam/course_name.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CourseRecorder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CourseName(),
              )),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'سجل الدورة',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: CostumeContainer(child: Text(''), color: Colors.white),
    );
  }
}
