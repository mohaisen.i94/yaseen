import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/bn_gradiant.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BNGradiant(
        child: Center(
      child: SizedBox(
        height: 300,
        width: 300,
        child: Image.asset(
          'images/imgimg.png',
          fit: BoxFit.cover,
        ),
      ),
    ));
  }
}
