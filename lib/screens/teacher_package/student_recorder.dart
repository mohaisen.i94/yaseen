import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class StudentRecorder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'سجل الطالب',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: CostumeContainer(
                  child: Text('اسم الطالب',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                )),
                CostumeImage(
                  radius: 90,
                  marginVir: 2,
                )
              ],
            ),
            CostumeContainer(
              marginVertical: 10,
              marginVerticalBottom: 0,
              color: Theme.of(context).primaryColor,
              height: 50,
              child: Text('تقرير الإنجاز',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
            ),
            Row(
              children: [
                CostumeContainer(
                  function: () {},
                  child: Text('من - إلى',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  width: 190,
                  height: 50,
                ),
                Expanded(
                    child: CostumeContainer(
                  function: () {},
                  child: Text('أسبوعي',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  marginHorizontal: 5,
                  height: 50,
                )),
                Expanded(
                    child: CostumeContainer(
                  function: () {},
                  child: Text('شهري',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  height: 50,
                )),
              ],
            ),
            Expanded(
              child: CostumeContainer(
                color: Colors.white,
                borderColor: Theme.of(context).primaryColor,
                child: Text(''),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    child: CostumeContainer(
                      height: 55,
                      color: Theme.of(context).primaryColor,
                      child: Text('الاختبارات',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white)),
                    ),
                  ),
                  SizedBox(width: 5),
                  Expanded(
                    child: CostumeContainer(
                      height: 55,
                      color: Theme.of(context).primaryColor,
                      child: Text('الحفظ الإجمالي',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white)),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
