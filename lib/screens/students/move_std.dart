import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/students/sms_with_phone.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class MoveStd extends StatefulWidget {
  @override
  State<MoveStd> createState() => _MoveStdState();
}

class _MoveStdState extends State<MoveStd> {
  List list = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'نقل مستخدم',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            DropDownMenuCostume(
                ctx: context, text: 'محفظ / طالب', height: 50, list: list),
            DropDownMenuCostume(
                ctx: context, text: 'محفظ / طالب', height: 50, list: list),
            CostumeContainer(
              radiusTop: 15,
              radiusBottom: 15,
              color: Colors.white,
              padding: 10,
              marginVertical: 10,
              marginVerticalBottom: 10,
              child: SmsWidget(text: 'بحث عن مستخدم', name: 'اسم الطالب كاملا'),
            ),
            CostumeContainer(
              color: Colors.white,
              radiusTop: 15,
              radiusBottom: 15,
              child: Column(
                children: [
                  Text(
                    'نقل إلى',
                    style: Theme.of(context)
                        .textTheme
                        .headline1!
                        .copyWith(color: Colors.black),
                  ),
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الإدارة / أسماء الحلقات',
                    height: 50,
                    list: list,
                    radius: 15,
                    margin: 15,
                  ),
                  SizedBox(height: 20),
                  CostumeContainer(
                    height: 50,
                    width: 200,
                    radiusBottom: 10,
                    radiusTop: 10,
                    marginVerticalBottom: 20,
                    color: Theme.of(context).primaryColor,
                    child: Text(
                      'موافق',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
