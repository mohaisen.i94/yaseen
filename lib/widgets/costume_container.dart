// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';

class CostumeContainer extends StatelessWidget {
  final Widget? child;

  final Color color;
  final double? width;
  final double? height;
  final double padding;
  final double? marginVertical;
  final double? marginVerticalBottom;
  final double? marginHorizontal;
  final Function()? function;
  final bool shadow;
  final double radiusBottom;
  final double radiusTop;
  final Color borderColor;
  final double borderWidth;
  final bool border;
  final Alignment alignment;

  const CostumeContainer({
    /*required*/ this.child,
    required this.color,
    this.width,
    this.height,
    this.padding = 0,
    this.marginVertical = 2.5,
    this.marginVerticalBottom = 2.5,
    this.marginHorizontal = 0,
    this.function,
    this.shadow = true,
    this.radiusBottom = 0,
    this.radiusTop = 0,
    this.borderColor = const Color(0xFFC8C8C8),
    this.borderWidth = .5,
    this.border = true,
    this.alignment = Alignment.center,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Container(
        // clipBehavior: Clip.none,
        margin: EdgeInsets.only(
            top: marginVertical!,
            bottom: marginVerticalBottom!,
            left: marginHorizontal!,
            right: marginHorizontal!),
        width: width,
        height: height,
        alignment: alignment,
        child: child,
        padding: EdgeInsets.all(padding),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(radiusBottom),
                bottomLeft: Radius.circular(radiusBottom),
                topRight: Radius.circular(radiusTop),
                topLeft: Radius.circular(radiusTop)),
            boxShadow: [
              shadow
                  ? BoxShadow(
                      color: Colors.black.withOpacity(.2),
                      offset: const Offset(0, 3),
                      blurRadius: 6)
                  : const BoxShadow()
            ],
            color: color,
            border: border
                ? Border.all(color: borderColor, width: borderWidth)
                : null),
      ),
    );
  }
}
