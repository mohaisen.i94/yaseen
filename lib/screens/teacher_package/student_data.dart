import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class StudentData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات الطالب',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: CostumeContainer(
                  child: Text(
                    'اسم الطالب',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                )),
                CostumeImage(
                  radius: 90,
                  marginVir: 2,
                )
              ],
            ),
            CostumeContainer(
              marginVertical: 5,
              marginVerticalBottom: 5,
              color: Colors.white,
              child: Text('خطة الطالب',
                  style: Theme.of(context).textTheme.bodyText1),
              height: 50,
            ),
            Row(
              children: [
                Expanded(
                  child: CostumeContainer(
                    color: Colors.white,
                    height: 50,
                    child: Text('سجل الطالب',
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: CostumeContainer(
                    height: 50,
                    color: Colors.white,
                    child: Text('إضافة تسميع',
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                )
              ],
            ),
            CostumeContainer(
              marginVertical: 10,
              color: Theme.of(context).primaryColor,
              child: Text('تسميع اليوم',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
              marginVerticalBottom: 0,
            ),
            CostumeContainer(
              color: Colors.white,
              child: Text('أدخل النص هنا',
                  style: Theme.of(context).textTheme.bodyText1),
              marginVerticalBottom: 0,
              marginVertical: 0,
              borderColor: Theme.of(context).primaryColor,
              height: 80,
            ),
            CostumeContainer(
              height: 50,
              color: Theme.of(context).primaryColor,
              child: Text('ملاحظات',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
              marginVerticalBottom: 0,
              marginVertical: 10,
            ),
            CostumeContainer(
              color: Colors.white,
              child: Text('أدخل النص هنا',
                  style: Theme.of(context).textTheme.bodyText1),
              marginVerticalBottom: 0,
              marginVertical: 0,
              borderColor: Theme.of(context).primaryColor,
              height: 120,
            ),
            CostumeContainer(
              color: Theme.of(context).primaryColor,
              child: Text('البريد',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
              marginVerticalBottom: 0,
              marginVertical: 20,
            ),
            CostumeContainer(
              color: Colors.white,
              child: Text('أدخل النص هنا',
                  style: Theme.of(context).textTheme.bodyText1),
              marginVerticalBottom: 0,
              marginVertical: 0,
              borderColor: Theme.of(context).primaryColor,
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}
