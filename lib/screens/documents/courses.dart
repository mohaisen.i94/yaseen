import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class CoursesReportScreen extends StatelessWidget {
  List list = [';odkfd', 'dfg', 'kjfjf'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'تقرير الدورات',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            DropDownMenuCostume(
              ctx: context,
              text: 'اسم الدورة',
              height: 45,
              list: list,
            ),
            const SizedBox(height: 15),
            Expanded(
                child: CostumeContainer(
              color: Colors.white,
              padding: 17,
              borderColor: Theme.of(context).primaryColor,
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        buildCostumeContainerTable(
                          height: 55,
                          color: Colors.amber[700]!,
                          child: Column(
                            children: const [Text('السبت'), Text('2020/12/20')],
                          ),
                        ),
                        buildCostumeContainerTable(
                            child: const Text('الغنة'),
                            color: Colors.deepOrange[400]!),
                        Expanded(
                          child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) =>
                                buildCostumeContainerTable(
                              child: const TextField(
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none),
                              ),
                              color: (index % 2 == 0)
                                  ? Colors.orange[50]!
                                  : Colors.orange[100]!,
                            ),
                            itemCount: 12,
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        buildCostumeContainerTable(
                          height: 55,
                          color: Colors.amber[700]!,
                          child: Column(
                            children: const [Text('السبت'), Text('2020/12/20')],
                          ),
                        ),
                        buildCostumeContainerTable(
                            child: const Text('المخارج'),
                            color: Colors.deepOrange[400]!),
                        Expanded(
                          child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) =>
                                buildCostumeContainerTable(
                              child: const TextField(
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none),
                              ),
                              color: (index % 2 == 0)
                                  ? Colors.orange[50]!
                                  : Colors.orange[100]!,
                            ),
                            itemCount: 12,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 130,
                    child: Column(
                      children: [
                        buildCostumeContainerTable(
                            child: const Text('الاسم'),
                            height: 90,
                            color: Colors.amber[700]!),
                        Expanded(
                          child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) =>
                                buildCostumeContainerTable(
                              child: const TextField(
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none),
                              ),
                              color: (index % 2 == 0)
                                  ? Colors.orange[50]!
                                  : Colors.orange[100]!,
                            ),
                            itemCount: 12,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }

  Widget buildCostumeContainerTable(
      {required Widget child, required Color color, double height = 35}) {
    return CostumeContainer(
      child: child,
      color: color,
      borderColor: Colors.white,
      height: height,
      marginVertical: 0,
      marginVerticalBottom: 0,
      padding: 0,
      shadow: false,
      borderWidth: .7,
    );
  }
}
