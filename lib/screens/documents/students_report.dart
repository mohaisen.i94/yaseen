import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class StudentsReport extends StatefulWidget {
  @override
  State<StudentsReport> createState() => _StudentsReportState();
}

class _StudentsReportState extends State<StudentsReport> {
  List list = [];

  List list2 = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'تقرير الطلاب / حلقة',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            DropDownMenuCostume(
                ctx: context, text: 'اختيار الحلقة', height: 50, list: list),
            DropDownMenuCostume(
                ctx: context,
                text: 'جميع الحلقة / اختيار طالب',
                height: 50,
                list: list2),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 1.5),
              child: Row(
                children: [
                  CostumeContainer(
                    function: () {},
                    child: Text('من - إلى',
                        style: Theme.of(context).textTheme.bodyText1),
                    color: Colors.white,
                    width: 190,
                    height: 50,
                  ),
                  Expanded(
                      child: CostumeContainer(
                    function: () {},
                    child: Text('أسبوعي',
                        style: Theme.of(context).textTheme.bodyText1),
                    color: Colors.white,
                    marginHorizontal: 5,
                    height: 50,
                  )),
                  Expanded(
                      child: CostumeContainer(
                    function: () {},
                    child: Text('شهري',
                        style: Theme.of(context).textTheme.bodyText1),
                    color: Colors.white,
                    height: 50,
                  )),
                ],
              ),
            ),
            CostumeContainer(
              child: Text('موافق',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white)),
              color: Theme.of(context).primaryColor,
              marginVertical: 3,
              marginVerticalBottom: 10,
              height: 50,
            ),
            CostumeContainer(
              child: Column(
                children: [
                  tableRow('السبت', '2020/21/22', 'محمد محمود'),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                        'يتم عرض الطلاب كما في الجدول أعلاه\n وعند اختيار طالب محدد يتم عرض '),
                  ),
                  tableRow2(),
                ],
              ),
              color: Colors.white,
              borderColor: Theme.of(context).primaryColor,
              padding: 5,
            ),
            // buildDataTable(),
          ],
        ),
      ),
    );
  }

  Widget tableRow(day, date, name) {
    return Column(
      children: [
        const SizedBox(height: 5),
        SizedBox(
          height: 50,
          child: ListView.builder(
            itemBuilder: (context, index) => index != 5 - 1
                ? buildCostumeContainerTable(
                    child: Column(
                      children: [
                        Text(day),
                        Text(
                          date,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(fontSize: 10, color: Colors.black),
                        ),
                      ],
                    ),
                    width: 84)
                : buildCostumeContainerTable(
                    child: const Text('الطالب'), padding: 3),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => index != 5 - 1
                ? buildCostumeContainerTable(
                    child: Text(day), color: Colors.orange[100]!, width: 84)
                : buildCostumeContainerTable(child: Text(name), padding: 5),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => index != 5 - 1
                ? buildCostumeContainerTable(
                    child: Text(day), color: Colors.orange[50]!, width: 84)
                : buildCostumeContainerTable(child: Text(name), padding: 5),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => index != 5 - 1
                ? buildCostumeContainerTable(
                    child: Text(day), color: Colors.orange[100]!, width: 84)
                : buildCostumeContainerTable(child: Text(name), padding: 5),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => index != 5 - 1
                ? buildCostumeContainerTable(
                    child: Text(day), color: Colors.orange[50]!, width: 84)
                : buildCostumeContainerTable(child: Text(name), padding: 5),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
      ],
    );
  }

  Widget tableRow2() {
    return Column(
      children: [
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => buildCostumeContainerTable(
                child: Text('ملاحظات'),
                width: 84,
                border: false,
                marginHorizontal: 1),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        const SizedBox(height: 5),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => buildCostumeContainerTable(
                child: Text(''),
                color: Colors.orange[100]!,
                width: 84,
                border: false,
                marginHorizontal: 1),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => buildCostumeContainerTable(
                child: Text(''),
                color: Colors.orange[50]!,
                width: 84,
                border: false,
                marginHorizontal: 1),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => buildCostumeContainerTable(
                child: Text(''),
                color: Colors.orange[100]!,
                width: 84,
                border: false,
                marginHorizontal: 1),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 35,
          child: ListView.builder(
            itemBuilder: (context, index) => buildCostumeContainerTable(
                child: Text(''),
                color: Colors.orange[50]!,
                width: 84,
                border: false,
                marginHorizontal: 1),
            itemCount: 5,
            scrollDirection: Axis.horizontal,
          ),
        ),
      ],
    );
  }

  Widget buildCostumeContainerTable({
    required Widget child,
    Color color = Colors.orange,
    double height = 40,
    double width = 90,
    double padding = 0,
    double marginHorizontal = 0,
    bool border = true,
  }) {
    return CostumeContainer(
      child: child,
      color: color,
      borderColor: Colors.black,
      height: height,
      marginVertical: 0,
      marginVerticalBottom: 0,
      padding: padding,
      shadow: false,
      borderWidth: .5,
      width: width,
      border: border,
      marginHorizontal: marginHorizontal,
    );
  }

// Widget buildDataTable() {
//   final columns = ['First Name', 'Last Name', 'Age'];
//
//   return DataTable(
//     columns: getColumns(columns),
//     rows: getRows(users),
//     dividerThickness: 1,
//     headingRowColor: MaterialStateProperty.resolveWith<Color>(
//         (Set<MaterialState> states) => Colors.orange
//         // Use the default value.
//         ),
//   );
// }
//
// List<DataColumn> getColumns(List<String> columns) {
//   return columns.map((String column) {
//     return DataColumn(
//       label: Text(column),
//     );
//   }).toList();
// }
//
// List<DataRow> getRows(List<User> users) => users.map((User user) {
//       final cells = [user.firstName, user.lastName, user.age];
//
//       return DataRow(
//         cells: Utils.modelBuilder(cells, (index, cell) {
//           return DataCell(
//             Text('$cell'),
//           );
//         }),
//       );
//     }).toList();
}
