import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class AddPlan extends StatelessWidget {
  List list = [];
  List<DataApp> data = [
    DataApp(mainText: 'أيام التحفيظ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'مقدار الحفظ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'بداية الحفظ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'نهاية الحفظ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'خطط جاهزة', subText: 'أدخل النص هنا'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'إضافة خطة',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    child: CostumeContainer(
                  child: Text('اسم الطالب',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                )),
                CostumeImage(
                  radius: 90,
                  marginVir: 2,
                )
              ],
            ),
            CostumeContainer(
              height: 50,
              marginVerticalBottom: 15,
              marginVertical: 20,
              color: Colors.white,
              child: Text('سجل الطالب',
                  style: Theme.of(context).textTheme.bodyText1),
            ),
            CostumeListView(data: data),
            Row(
              children: [
                Expanded(
                    child: DropDownMenuCostume(
                        ctx: context,
                        text: 'اختيار',
                        height: 55,
                        radius: 5,
                        list: list)),
                CostumeContainer(
                    width: 140,
                    height: 55,
                    child: Text(
                      'خطط جاهزة',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                    color: Theme.of(context).primaryColor)
              ],
            ),
            CostumeContainer(
              marginVertical: 15,
              height: 55,
              color: Colors.white,
              child: Text('معاينة / تحرير يدوي',
                  style: Theme.of(context).textTheme.bodyText1),
            )
          ],
        ),
      ),
    );
  }
}
