import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class DataForParents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات الطالب للأهل',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                CostumeContainer(
                  child: Text(
                    'المحفظ',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                  width: 85,
                ),
                SizedBox(width: 5),
                Expanded(
                    child: CostumeContainer(
                  child: Text(
                    'اسم الطالب',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  color: Color(0xFFDCFCC6),
                  height: 55,
                )),
                CostumeImage(
                  radius: 90,
                  marginVir: 2,
                )
              ],
            ),
            CostumeContainer(
              child: Text(
                'خطة الطالب',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              color: Colors.white,
              height: 55,
              marginVerticalBottom: 5,
              marginVertical: 10,
            ),
            Row(
              children: [
                Expanded(
                  child: CostumeContainer(
                    color: Colors.white,
                    height: 50,
                    child: Text('سجل الطالب',
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: CostumeContainer(
                    height: 50,
                    color: Colors.white,
                    child: Text('الحفظ الإجمالي',
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                )
              ],
            ),
            CostumeContainer(
              color: Theme.of(context).primaryColor,
              marginVerticalBottom: 0,
              marginVertical: 5,
              child: Text(
                'آخر تسميع',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
            ),
            CostumeContainer(
              height: 100,
              marginVertical: 0,
              borderColor: Theme.of(context).primaryColor,
              color: Colors.white,
              child: Text('عرض آخر تسميع',
                  style: Theme.of(context).textTheme.bodyText1),
            ),
            CostumeContainer(
              color: Theme.of(context).primaryColor,
              marginVerticalBottom: 0,
              marginVertical: 10,
              child: Text(
                'الملاحظات',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white),
              ),
            ),
            CostumeContainer(
              height: 100,
              marginVertical: 0,
              borderColor: Theme.of(context).primaryColor,
              color: Colors.white,
              child: Text('عرض الملاحظات',
                  style: Theme.of(context).textTheme.bodyText1),
            ),
            Row(
              children: [
                Expanded(
                  child: CostumeContainer(
                    color: Colors.white,
                    height: 50,
                    child: Text('مراسلة الإدارة',
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: CostumeContainer(
                    height: 50,
                    color: Colors.white,
                    child: Text('مراسلة المحفظ',
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                )
              ],
            ),
            CostumeContainer(
              height: 60,
              marginVertical: 40,
              color: Color(0xFFDBE8D3),
              child: Text('المكتبة والأنشطة',
                  style: Theme.of(context).textTheme.bodyText1),
            ),
          ],
        ),
      ),
    );
  }
}
