import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/circles/circle_children.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class AddResultTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'إضافة نتائج الاختبار',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Column(
          children: [
            const SizedBox(height: 30),
            Row(
              children: [
                Expanded(
                    child: CostumeContainer(
                  child: Text('اختبار نصفي',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: Colors.white,
                  height: 55,
                  shadow: false,
                )),
                const SizedBox(width: 5),
                CostumeContainer(
                  child: Text('العنوان',
                      style: Theme.of(context).textTheme.bodyText1),
                  color: const Color(0xFFDCFCC6),
                  height: 55,
                  width: 100,
                )
              ],
            ),
            const SizedBox(height: 8),
            const Expanded(
                child: BuildRowListView(
                    visible: false, status: '30', name: 'اسم الطالب'))
          ],
        ),
      ),
    );
  }
}
