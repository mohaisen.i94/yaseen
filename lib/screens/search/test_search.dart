import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/circles/circle_data.dart';

class TestSearchScreen extends StatelessWidget {
  List<DataApp> data = [
    DataApp(mainText: 'الحلقة', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الاسم', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الجزء', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'التاريخ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المختبر', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'العلامة', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'التقدير', subText: 'أدخل النص هنا'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'الاختبارات',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: CostumeBody(
          data: data,
          text: 'الملاحظات والتوصيات',
          subText: 'ترسل للمحفظ والأهل والإدارة',
        ),
      ),
    );
  }
}
