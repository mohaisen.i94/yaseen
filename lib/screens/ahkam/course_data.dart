import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_course.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_std_data.dart';
import 'package:yaseen_app/screens/circles/circle_data.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CourseData extends StatelessWidget {
  List<DataApp> data = [
    DataApp(mainText: 'مستوى الدورة', subText: 'عليا'),
    DataApp(mainText: 'أيام الدورة', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الفئة / الصـف', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المدرس', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المساعــد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'تاريخ البـدء', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'عدد الطلاب', subText: 'أدخل النص هنا'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AhkamCourse(),
              )),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات الدورة',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
          child: Column(
            children: [
              CostumeListView(data: data),
              CostumeContainer(
                function: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AhkamStdData(),
                    )),
                child: Text('إضافة طالب جديد',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white)),
                color: Theme.of(context).primaryColor,
                height: 50,
                marginVerticalBottom: 15,
              ),
              CostumeContainer(
                height: 50,
                marginVerticalBottom: 0,
                color: Theme.of(context).primaryColor,
                child: Text('طلاب الحلقة',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white)),
              ),
              BuildTextField(
                text: 'يتم عرض طلاب الحلقة هنا',
                textAlign: TextAlign.center,
                borderColor: Theme.of(context).primaryColor,
              )
            ],
          ),
        ),
      ),
    );
  }
}
