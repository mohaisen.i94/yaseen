import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class Tasmee9 extends StatelessWidget {
  List list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'إضافة تسميع',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              CostumeContainer(
                height: 50,
                radiusBottom: 5,
                radiusTop: 5,
                color: Theme.of(context).primaryColor,
                child: Text(
                  'إضافة تسميع جديد',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: Colors.white),
                ),
              ),
              Row(
                children: [
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الآية',
                    height: 50,
                    list: list,
                    radius: 5,
                    width: 130,
                  ),
                  Expanded(
                    child: DropDownMenuCostume(
                      ctx: context,
                      text: 'البداية',
                      height: 50,
                      list: list,
                      radius: 5,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الآية',
                    height: 50,
                    list: list,
                    radius: 5,
                    width: 130,
                  ),
                  Expanded(
                    child: DropDownMenuCostume(
                      ctx: context,
                      text: 'البداية',
                      height: 50,
                      list: list,
                      radius: 5,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  CostumeContainer(
                    height: 50,
                    width: 130,
                    radiusTop: 5,
                    radiusBottom: 5,
                    marginHorizontal: 2,
                    color: Theme.of(context).primaryColor,
                    child: Text('موافق',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white)),
                  ),
                  Expanded(
                    child: DropDownMenuCostume(
                      ctx: context,
                      text: 'التقدير',
                      height: 50,
                      list: list,
                      radius: 5,
                    ),
                  ),
                ],
              ),
              CostumeContainer(
                height: 50,
                radiusTop: 5,
                radiusBottom: 5,
                marginHorizontal: 2,
                marginVertical: 15,
                color: Theme.of(context).primaryColor,
                child: Text('مراجعة ما تم حفظه',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white)),
              ),
              Row(
                children: [
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الآية',
                    height: 50,
                    list: list,
                    radius: 5,
                    width: 130,
                  ),
                  Expanded(
                    child: DropDownMenuCostume(
                      ctx: context,
                      text: 'البداية',
                      height: 50,
                      list: list,
                      radius: 5,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الآية',
                    height: 50,
                    list: list,
                    radius: 5,
                    width: 130,
                  ),
                  Expanded(
                    child: DropDownMenuCostume(
                      ctx: context,
                      text: 'البداية',
                      height: 50,
                      list: list,
                      radius: 5,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  CostumeContainer(
                    height: 50,
                    width: 130,
                    radiusTop: 5,
                    radiusBottom: 5,
                    marginHorizontal: 2,
                    color: Theme.of(context).primaryColor,
                    child: Text('موافق',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.white)),
                  ),
                  Expanded(
                    child: DropDownMenuCostume(
                      ctx: context,
                      text: 'التقدير',
                      height: 50,
                      list: list,
                      radius: 5,
                    ),
                  ),
                ],
              ),
              CostumeContainer(
                height: 45,
                marginVertical: 25,
                marginVerticalBottom: 0,
                color: Theme.of(context).primaryColor,
                child: Text('الملاحظات',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white)),
              ),
              BuildTextField(
                text: 'أدخل النص هنا',
                textAlign: TextAlign.center,
                borderColor: Theme.of(context).primaryColor,
              )
            ],
          ),
        ),
      ),
    );
  }
}
