import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CostumeUserAndPassowrd extends StatelessWidget {
  CostumeUserAndPassowrd(
      {required this.passwordFunction, required this.userFunction});
  Function() passwordFunction;
  Function() userFunction;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            child: CostumeContainer(
          function: passwordFunction,
          child:
              Text('كلمة المرور', style: Theme.of(context).textTheme.bodyText1),
          color: Colors.white,
          height: 50,
          marginHorizontal: 1.5,
          shadow: false,
        )),
        Expanded(
            child: CostumeContainer(
          function: userFunction,
          child: Text('اسم المستخدم',
              style: Theme.of(context).textTheme.bodyText1),
          color: Colors.white,
          height: 50,
          marginHorizontal: 1.5,
          shadow: false,
        ))
      ],
    );
  }
}
