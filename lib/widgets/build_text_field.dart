import 'package:flutter/material.dart';

class BuildTextField extends StatelessWidget {
  const BuildTextField(
      {Key? key,
      required this.text,
      this.textAlign = TextAlign.end,
      this.borderColor = const Color(0xFFC8C8C8),
      this.height})
      : super(key: key);

  final String text;
  final TextAlign textAlign;
  final Color borderColor;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      // margin: const EdgeInsets.all(2),
      child: TextField(
        style: Theme.of(context).textTheme.bodyText1,
        textDirection: TextDirection.ltr,
        textAlign: textAlign,
        minLines: 1,
        maxLines: 5,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(15),
          filled: true,
          fillColor: Colors.white,
          hintText: text,
          hintStyle: Theme.of(context).textTheme.bodyText1,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: borderColor,
          )),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: borderColor,
          )),
        ),
      ),
    );
  }
}
