import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/circles/circle_children.dart';
import 'package:yaseen_app/screens/management/new_teacher.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class Teacher2 extends StatefulWidget {
  Teacher2({Key? key}) : super(key: key);

  @override
  State<Teacher2> createState() => _Teacher2State();
}

class _Teacher2State extends State<Teacher2> {
  var status;
  bool starColor = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    status = 'حاضر';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'المحفظين',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Column(
          children: [
            BuildAbsenceWidget(context: context, absence: '10', presence: '25'),
            BuildRichTextDayAndDate(
                context: context, day: 'السبت', date: '24/12/20'),
            Expanded(
                child: BuildRowListView(
              visible: false,
              status: status,
              name: 'اسم المحفظ',
              visibleStar: false,
            ))
          ],
        ),
      ),
    );
  }
}
