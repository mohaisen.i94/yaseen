import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class MotivationStudent extends StatelessWidget {
  List list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'تسجيل الحوافز',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            CostumeContainer(
              padding: 10,
              color: Color(0xFFF5F5F5),
              borderWidth: 1.5,
              radiusTop: 8,
              radiusBottom: 8,
              borderColor: Colors.white,
              child: Column(
                children: [
                  SizedBox(height: 10),
                  DropDownMenuCostume(
                    ctx: context,
                    text: 'الاسم',
                    height: 50,
                    list: list,
                    radius: 8,
                  ),
                  Row(
                    children: [
                      CostumeContainer(
                        color: Colors.white,
                        height: 50,
                        width: 170,
                        borderWidth: 1,
                        radiusBottom: 8,
                        radiusTop: 8,
                        child: Text(
                          'التاريخ',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        child: CostumeContainer(
                          color: Colors.white,
                          height: 50,
                          borderWidth: 1,
                          radiusBottom: 8,
                          radiusTop: 8,
                          child: Text(
                            'التاريخ',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                      ),
                    ],
                  ),
                  CostumeContainer(
                    height: 50,
                    radiusTop: 8,
                    radiusBottom: 8,
                    marginVerticalBottom: 15,
                    marginVertical: 10,
                    color: Theme.of(context).primaryColor,
                    child: Text(
                      'موافق',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            CostumeContainer(color: Colors.white)
          ],
        ),
      ),
    );
  }
}
