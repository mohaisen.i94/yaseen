import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';
import 'package:yaseen_app/widgets/drop_down.dart';

class SMSWithPhone extends StatefulWidget {
  @override
  State<SMSWithPhone> createState() => _SMSWithPhoneState();
}

class _SMSWithPhoneState extends State<SMSWithPhone> {
  List list = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'إرسال رسالة جوال',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              DropDownMenuCostume(
                  ctx: context,
                  text: 'الحلقات / الدورات',
                  height: 50,
                  list: list),
              SmsWidget(
                text: 'بحث عن طالب',
                name: 'اسم الطالب كاملا',
              ),
              SizedBox(height: 10),
              messageText(context,
                  'ا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا ا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا الله       لا إله إلا  '),
              SizedBox(height: 20),
              Row(
                children: [
                  CostumeContainer(
                    height: 55,
                    width: 85,
                    color: Theme.of(context).primaryColor,
                    child: Text(
                      'إرسال',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: CostumeContainer(
                      height: 55,
                      color: Colors.white,
                      child: Text('0215552222'),
                    ),
                  ),
                  CostumeContainer(
                    height: 55,
                    width: 85,
                    color: Theme.of(context).primaryColor,
                    child: Text(
                      'الجوال',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class SmsWidget extends StatefulWidget {
  final String text;
  final String name;

  SmsWidget({required this.text, required this.name});

  @override
  State<SmsWidget> createState() => _SmsWidgetState();
}

class _SmsWidgetState extends State<SmsWidget> {
  bool _value = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CostumeContainer(
          height: 45,
          marginVertical: 15,
          marginVerticalBottom: 0,
          color: const Color(0xFFF1F8ED),
          child: Row(
            children: [
              Spacer(),
              Text(widget.text, style: Theme.of(context).textTheme.bodyText1),
              Spacer(flex: 3),
              Text(
                '|       الجميع',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.check_circle,
                    color: Theme.of(context).primaryColor,
                  ))
            ],
          ),
        ),
        SizedBox(
          height: 250,
          child: ListView.builder(
            itemBuilder: (context, index) => CostumeContainer(
              color: Colors.white,
              borderWidth: .25,
              height: 45,
              marginVertical: 0,
              marginVerticalBottom: 0,
              child: CheckboxListTile(
                value: _value,
                onChanged: (onChanged) {
                  _value = onChanged!;
                  setState(() {});
                },
                title: Text(widget.name),
                activeColor: Theme.of(context).primaryColor,
                controlAffinity: ListTileControlAffinity.leading,
              ),
            ),
            itemCount: 10,
          ),
        )
      ],
    );
  }
}

Widget messageText(BuildContext ctx, message) {
  return Column(
    children: [
      CostumeContainer(
        height: 50,
        marginVerticalBottom: 0,
        child: Text(
          'نص الرسالة',
          style: Theme.of(ctx).textTheme.headline1!.copyWith(
              color: Colors.black, fontWeight: FontWeight.normal, fontSize: 20),
        ),
        color: Color(0xFFF1F8ED),
      ),
      SizedBox(
          height: 200,
          child: CostumeContainer(
            marginVertical: 0,
            color: Colors.white,
            child: SingleChildScrollView(child: Text(message)),
          ))
    ],
  );
}
