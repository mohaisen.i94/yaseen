import 'package:flutter/material.dart';

class DropDownMenuCostume extends StatefulWidget {
  DropDownMenuCostume(
      {Key? key,
      required this.ctx,
      required this.text,
      required this.height,
      this.width,
      this.radius = 0,
      this.dropDownChoose,
      required this.list,
      this.icon = const Icon(Icons.keyboard_arrow_down_sharp),
      this.color = Colors.white,
      this.margin = 2})
      : super(key: key);

  final BuildContext ctx;
  final String text;
  final double height;
  final double? width;
  final double? radius;
  final List list;
  final Widget? icon;
  final Color? color;
  final double margin;
  var dropDownChoose;

  @override
  State<DropDownMenuCostume> createState() => _DropDownMenuCostumeState();
}

class _DropDownMenuCostumeState extends State<DropDownMenuCostume> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(widget.radius!),
        border: Border.all(color: const Color(0xFFC8C8C8)),
        color: widget.color,
      ),
      padding: const EdgeInsets.only(left: 30, right: 10),
      margin: EdgeInsets.all(widget.margin),
      child: DropdownButton(
        underline: const Text(''),
        hint: Text(
          widget.text,
          style:
              Theme.of(widget.ctx).textTheme.bodyText1!.copyWith(fontSize: 17),
        ),
        icon: widget.icon,
        isExpanded: true,
        value: widget.dropDownChoose,
        onChanged: (value) {
          widget.dropDownChoose = value;
          setState(() {});
        },
        items: widget.list
            .map((e) => DropdownMenuItem(
                  onTap: () {},
                  child: Text('$e'),
                  value: e,
                ))
            .toList(),
      ),
    );
  }
}
