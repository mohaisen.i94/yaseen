import 'package:flutter/material.dart';

class BNGradiant extends StatelessWidget {
  final Widget child;

  const BNGradiant({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: child,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xFFF1F8ED), Color(0xFFF6E5B9)],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
        ),
      ],
    );
  }
}
