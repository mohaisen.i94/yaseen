import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:yaseen_app/screens/ahkam/add_result_test.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_course.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_recorder_std.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_std_data.dart';
import 'package:yaseen_app/screens/ahkam/course_data.dart';
import 'package:yaseen_app/screens/ahkam/course_name.dart';
import 'package:yaseen_app/screens/ahkam/course_recorder.dart';
import 'package:yaseen_app/screens/circles/circle_children.dart';
import 'package:yaseen_app/screens/circles/circle_data.dart';
import 'package:yaseen_app/screens/circles/circle_main.dart';
import 'package:yaseen_app/screens/documents/courses.dart';
import 'package:yaseen_app/screens/documents/document.dart';
import 'package:yaseen_app/screens/documents/financial.dart';
import 'package:yaseen_app/screens/documents/present_all.dart';
import 'package:yaseen_app/screens/documents/students_report.dart';
import 'package:yaseen_app/screens/documents/teacher_recorder.dart';
import 'package:yaseen_app/screens/documents/tests.dart';
import 'package:yaseen_app/screens/landing_page.dart';
import 'package:yaseen_app/screens/landing_page2.dart';
import 'package:yaseen_app/screens/management/management.dart';
import 'package:yaseen_app/screens/management/new_student.dart';
import 'package:yaseen_app/screens/management/new_teacher.dart';
import 'package:yaseen_app/screens/documents/motivation.dart';
import 'package:yaseen_app/screens/on_boarding.dart';
import 'package:yaseen_app/screens/search/chat/build_listview_chat.dart';
import 'package:yaseen_app/screens/search/chat/chat.dart';
import 'package:yaseen_app/screens/search/chat/chat_screen.dart';
import 'package:yaseen_app/screens/search/chat/message_bubble.dart';
import 'package:yaseen_app/screens/search/financial_search.dart';
import 'package:yaseen_app/screens/search/search_main.dart';
import 'package:yaseen_app/screens/search/test_search.dart';
import 'package:yaseen_app/screens/setting.dart';
import 'package:yaseen_app/screens/splash_screen.dart';
import 'package:yaseen_app/screens/student_package/data_for_parents.dart';
import 'package:yaseen_app/screens/student_package/teacher_data_for_std.dart';
import 'package:yaseen_app/screens/students/enter_ads.dart';
import 'package:yaseen_app/screens/students/library.dart';
import 'package:yaseen_app/screens/students/motivation_student.dart';
import 'package:yaseen_app/screens/students/move_std.dart';
import 'package:yaseen_app/screens/students/permissions.dart';
import 'package:yaseen_app/screens/students/sms_with_phone.dart';
import 'package:yaseen_app/screens/students/students_screen.dart';
import 'package:yaseen_app/screens/students/users_screen.dart';
import 'package:yaseen_app/screens/teacher_package/add_plan.dart';
import 'package:yaseen_app/screens/teacher_package/add_plan_manually.dart';
import 'package:yaseen_app/screens/teacher_package/student_data.dart';
import 'package:yaseen_app/screens/teacher_package/student_recorder.dart';
import 'package:yaseen_app/screens/teacher_package/student_test2.dart';
import 'package:yaseen_app/screens/teacher_package/tasmee9.dart';
import 'package:yaseen_app/screens/teacher_package/view_screen.dart';
import 'package:yaseen_app/screens/teachers/teacher2.dart';
import 'package:yaseen_app/screens/teachers/teacher_data.dart';
import 'package:yaseen_app/screens/teachers/teachers.dart';
import 'package:yaseen_app/screens/student_package/total_saved.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
          BouncingScrollWrapper.builder(context, widget!),
          maxWidth: 1200,
          minWidth: 480,
          defaultScale: true,
          breakpoints: [
            const ResponsiveBreakpoint.resize(480, name: MOBILE),
            const ResponsiveBreakpoint.autoScale(800, name: TABLET),
            const ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
          background: Container(color: const Color(0xFFF5F5F5))),
      themeMode: ThemeMode.light,
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textSelectionTheme: const TextSelectionThemeData(
          cursorColor: Colors.grey,
        ),
        // primarySwatch: Colors.green,
        primaryColor: Color(0xFF70AD47),
        textTheme: const TextTheme(
            headline1: TextStyle(
                color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),
            headline2: TextStyle(
                color: Color(0xFF707070),
                fontSize: 20,
                fontWeight: FontWeight.bold),
            bodyText1: TextStyle(
              color: Color(0xFF707070),
              fontSize: 18,
            )),
        canvasColor: const Color(0xFFEEEEEE),

        // fontFamily: ''
      ),
      home: SplashScreen(),
    );
  }
}
