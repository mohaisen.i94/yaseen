import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class TeacherAndStdName extends StatelessWidget {
  TeacherAndStdName({required this.name, required this.text, this.function});
  final String name;
  final String text;
  final Function()? function;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CostumeContainer(
          function: function,
          child: Text(
            text,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          color: const Color(0xFFDCFCC6),
          width: 100,
          height: 50,
        ),
        const SizedBox(width: 5),
        Expanded(
          child: CostumeContainer(
            child: Text(
              name,
              style: Theme.of(context).textTheme.bodyText1,
            ),
            color: const Color(0xFFDCFCC6),
            height: 50,
          ),
        ),
        CostumeImage(
          radius: 80,
          marginVir: 15,
        ),
      ],
    );
  }
}
