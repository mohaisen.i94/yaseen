import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/ahkam/ahkam_recorder_std.dart';
import 'package:yaseen_app/screens/ahkam/course_name.dart';
import 'package:yaseen_app/screens/circles/circle_data.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/screens/management/widgets/teacher_and_std_name.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class AhkamStdData extends StatelessWidget {
  List<DataApp> data = [
    DataApp(mainText: 'الاسم ثلاثي', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'تاريخ الميلاد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'العمر', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'السكن', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الجوال', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'دورة الأحكام', subText: 'اختار من القائمة'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CourseName(),
              )),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'بيانات طالب الأحكام',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            children: [
              TeacherAndStdName(
                  function: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CourseName(),
                      )),
                  name: 'اسم الطالب',
                  text: 'الدورة'),
              CostumeContainer(
                function: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AhkamRecorderStd(),
                    )),
                child: Text(
                  'سجل الطالب',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                color: Colors.white,
                height: 55,
                marginVerticalBottom: 15,
              ),
              CostumeListView(data: data),
              const SizedBox(height: 9),
              CostumeContainer(
                height: 50,
                marginVerticalBottom: 0,
                color: Theme.of(context).primaryColor,
                child: Text('ملاحظات',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: Colors.white)),
              ),
              BuildTextField(
                text: 'أدخل النص هنا',
                textAlign: TextAlign.center,
                borderColor: Theme.of(context).primaryColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
