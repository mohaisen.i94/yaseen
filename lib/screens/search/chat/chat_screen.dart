import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/search/chat/build_listview_chat.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class ChatScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'البريد',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: CostumeContainer(
        color: Colors.white,
        radiusBottom: 5,
        radiusTop: 5,
        marginHorizontal: 20,
        marginVerticalBottom: 20,
        marginVertical: 5,
        child: Column(
          children: [
            Expanded(child: BuildListViewChat()),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Row(
                children: [
                  CostumeContainer(
                    color: Theme.of(context).primaryColor,
                    height: 50,
                    width: 50,
                    radiusTop: 30,
                    padding: 6,
                    radiusBottom: 30,
                    child: IconButton(
                        color: Colors.white,
                        onPressed: () {},
                        icon: Icon(
                          Icons.send,
                        )),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: CostumeContainer(
                        color: Colors.grey[400]!,
                        radiusTop: 30,
                        radiusBottom: 30,
                        child: Row(
                          children: [
                            Text('      '),
                            Expanded(
                              child: TextField(
                                decoration: InputDecoration(
                                    hintText: 'write message',
                                    enabledBorder: InputBorder.none),
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
