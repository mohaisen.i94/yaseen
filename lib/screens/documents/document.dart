import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/documents/courses.dart';
import 'package:yaseen_app/screens/documents/financial.dart';
import 'package:yaseen_app/screens/documents/motivation.dart';
import 'package:yaseen_app/screens/documents/present_all.dart';
import 'package:yaseen_app/screens/documents/students_report.dart';
import 'package:yaseen_app/screens/documents/teacher_recorder.dart';
import 'package:yaseen_app/screens/documents/tests.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class DocumentScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List data = [
      DataApp(
          mainText: 'دوام المحفظين',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TeacherRecorder(),
              ))),
      DataApp(
          mainText: 'الحوافز',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MotivationScreen(),
              ))),
      DataApp(
          mainText: 'أعداد الحضور',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => PresentScreen(),
              ))),
      DataApp(
          mainText: 'التقرير المالي',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FinancialReport(),
              ))),
      DataApp(
          mainText: 'تقرير حلقة',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => StudentsReport(),
              ))),
      DataApp(
          mainText: 'الاختبارات',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TestScreen(),
              ))),
      DataApp(
          mainText: 'دورات الأحكام',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CoursesReportScreen(),
              ))),
    ];
    ////////////////
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'التقارير',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        child: Column(
          children: [
            const SizedBox(height: 8),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, index) => CostumeContainer(
                padding: 0,
                height: 70,
                marginHorizontal: 15,
                child: Text(data[index].mainText,
                    style: Theme.of(context).textTheme.bodyText1),
                color: data[index].color,
                function: data[index].function,
              ),
            )
          ],
        ),
      ),
    );
  }
}
