import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class Chat1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_back),
          ),
          centerTitle: true,
          title: Text(
            'البريد',
            style: Theme.of(context).textTheme.headline1,
          ),
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              CostumeContainer(
                color: Theme.of(context).primaryColor,
                padding: 0,
                marginVerticalBottom: 0,
                child: TabBar(
                    indicator: const BoxDecoration(color: Color(0xFF82C457)),
                    unselectedLabelColor: Theme.of(context).primaryColor,
                    tabs: [
                      Tab(
                        child: Text(
                          'الصادر',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white),
                        ),
                      ),
                      Tab(
                        child: Text(
                          'الوارد',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.white),
                        ),
                      )
                    ]),
              ),
              Expanded(
                child: CostumeContainer(
                  marginVertical: 0,
                  color: Colors.white,
                  borderColor: Theme.of(context).primaryColor,
                  child: TabBarView(
                    children: [costumeChat(context), costumeChat(context)],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget costumeChat(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        child: Row(
          children: [
            CircleAvatar(
              radius: 4,
              backgroundColor: Theme.of(context).primaryColor,
            ),
            const Spacer(),
            Column(
              children: const [
                Text('أحمد بشير نشوان'),
                Text('آخر رسالة وصلتك')
              ],
            ),
            const CircleAvatar(
              radius: 24,
              backgroundImage: AssetImage('images/img.png'),
            )
          ],
        ),
      ),
      itemCount: 5,
    );
  }
}
