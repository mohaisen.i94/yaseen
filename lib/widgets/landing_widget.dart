import 'package:flutter/material.dart';
import 'package:yaseen_app/screens/management/widgets/costume_image.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class LandingWidget extends StatelessWidget {
  final String date;
  final String name;
  final Color color;
  final Color smallColor;
  final double width;
  const LandingWidget({
    Key? key,
    required this.date,
    required this.name,
    this.color = Colors.white,
    this.smallColor = Colors.orange,
    this.width = 250,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CostumeContainer(
      // height: 129.4,
      width: width,
      marginHorizontal: 5,
      color: color,
      radiusBottom: 15,
      radiusTop: 15,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              padding: const EdgeInsets.all(5),
              height: 30,
              width: 90,
              child: Text(
                date,
                textAlign: TextAlign.center,
              ),
              decoration: BoxDecoration(
                  color: smallColor,
                  borderRadius: BorderRadiusDirectional.only(
                      topStart: Radius.circular(15),
                      bottomEnd: Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(.16),
                      blurRadius: 6,
                      offset: Offset(0, 3),
                    )
                  ]),
            ),
          ),
          Positioned(
              top: 0,
              right: 0,
              child: SizedBox(
                  height: (65),
                  width: (62),
                  child: Image.asset(
                    'images/imgB.png',
                    fit: BoxFit.cover,
                  ))),
          Positioned(
            top: 20,
            right: 0,
            left: 0,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 40),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                          margin: const EdgeInsets.only(top: 22),
                          height: (60),
                          width: (55),
                          child: Image.asset(
                            'images/imgT.png',
                            fit: BoxFit.cover,
                          )),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: smallColor,
                        ),
                        padding: EdgeInsets.only(bottom: 7),
                        child: CostumeImage(
                          radius: 75,
                          marginVir: 0,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Text(
                  'الطالب المميز لشهر محرم',
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      height: .5),
                  textAlign: TextAlign.center,
                ),
                Text(
                  name,
                  style: Theme.of(context).textTheme.bodyText1,
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
          Positioned(
            right: 0,
            top: 0,
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                color: smallColor,
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(7),
                    bottomRight: Radius.circular(7)),
              ),
              width: (7),
            ),
          )
        ],
      ),
    );
  }
}
