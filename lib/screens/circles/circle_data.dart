import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/management/widgets/costume_listview.dart';
import 'package:yaseen_app/widgets/build_text_field.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CircleDataScreen extends StatelessWidget {
  List<DataApp> data = [
    DataApp(mainText: 'اسم الحلقة', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'أيام التحفيظ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'الفئة / الصف', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المحفظ', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المساعد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المساعد', subText: 'أدخل النص هنا'),
    DataApp(mainText: 'المساعد', subText: 'أدخل النص هنا'),
  ];
  CircleDataScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'بيانات الحلقة / جديدة',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: CostumeBody(
          data: data,
          text: 'طلاب الحلقة',
          subText: 'يتم عرض طلاب الحلقة هنا',
        ),
      ),
    );
  }
}

class CostumeBody extends StatelessWidget {
  List data;
  String text;
  String subText;

  CostumeBody(
      {Key? key, required this.data, required this.text, required this.subText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 25),
          CostumeListView(data: data),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                child: CostumeContainer(
                  height: 50,
                  marginVerticalBottom: 0,
                  color: Theme.of(context).primaryColor,
                  child: Text('طلاب الحلقة',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Colors.white)),
                ),
              ),
            ],
          ),
          BuildTextField(
            text: subText,
            textAlign: TextAlign.center,
            borderColor: Theme.of(context).primaryColor,
          ),
          const SizedBox(height: 8),
          CostumeContainer(
            child: Text('موافق',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.white)),
            color: Theme.of(context).primaryColor,
            height: 50,
          ),
        ],
      ),
    );
  }
}
