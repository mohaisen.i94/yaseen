import 'package:flutter/material.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/circles/circle_main.dart';
import 'package:yaseen_app/screens/documents/document.dart';
import 'package:yaseen_app/screens/management/new_student.dart';
import 'package:yaseen_app/screens/management/new_teacher.dart';
import 'package:yaseen_app/screens/search/chat/chat.dart';
import 'package:yaseen_app/screens/search/financial_search.dart';
import 'package:yaseen_app/screens/search/search_main.dart';
import 'package:yaseen_app/screens/search/test_search.dart';
import 'package:yaseen_app/screens/teachers/teachers.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class ManagementScreen extends StatelessWidget {
  const ManagementScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<DataApp> data = [
      DataApp(
          mainText: 'المحفظين',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'الحلقات',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleMainScreen(),
              ))),
      DataApp(
          mainText: 'طالب جديد',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NewStudentScreen(),
              ))),
      DataApp(
          mainText: 'محفظ جديد',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NewTeacherScreen(),
              ))),
      DataApp(
          mainText: 'بحث',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SearchMainScreen(),
              ))),
      DataApp(
          mainText: 'تقارير',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DocumentScreen(),
              ))),
      DataApp(
          mainText: 'دورات الأحكام',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'اختبارات',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TestSearchScreen(),
              ))),
      DataApp(
          mainText: 'المالية',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FinancialSearchScreen(),
              ))),
      DataApp(
          mainText: 'الرسائل',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Chat1(),
              ))),
      DataApp(
          mainText: 'الإعلانات',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'الحفظة',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'حوافز المحفظين',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'التواصل بالجوال',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'الإعدادات',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'نقل مستخدم',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'الصلاحيات',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
      DataApp(
          mainText: 'المستخدمون',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const TeacherScreen(),
              ))),
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text('الإدارة', style: Theme.of(context).textTheme.headline1),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.arrow_back),
        ),
      ),
      body: buildGridView(data),
    );
  }
}

Widget buildGridView(data) {
  return GridView.builder(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 5,
        mainAxisSpacing: 5,
        childAspectRatio: 2.5,
      ),
      itemCount: 18,
      itemBuilder: (context, index) {
        return CostumeContainer(
            padding: 0,
            height: 70,
            function: data[index].function,
            child: Text(data[index].mainText,
                style: Theme.of(context).textTheme.bodyText1),
            color: data[index].color!);
      });
}
