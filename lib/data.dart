import 'package:flutter/cupertino.dart';

class DataApp {
  String mainText;
  String? subText;
  Widget? widget;
  Color? color;
  Function()? function;
  DataApp(
      {required this.mainText,
      this.subText,
      this.widget,
      this.color,
      this.function});
}
