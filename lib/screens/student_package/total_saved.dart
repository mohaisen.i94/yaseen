import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

class TotalSaved extends StatelessWidget {
  bool done = true; // to Specified the color of the listView element
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'إجمالي الحفظ',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Colors.white, Color(0xFFC8EBD9)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 170,
                    child: ListView.builder(
                      itemBuilder: (context, index) => Container(
                        alignment: Alignment.center,
                        height: 220,
                        child: Text('${index + 1}'),
                        decoration: BoxDecoration(
                            color: done ? Color(0xFFAADDFF) : null,
                            border: Border.all(color: Colors.red, width: 2)),
                      ),
                      itemCount: 30,
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                          Color(0xFFF9FDFB),
                          Color(0xFFF8FCFA).withOpacity(0)
                        ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter)),
                    height: 100,
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                          Color(0xFFF8FCFA).withOpacity(0),
                          Color(0xFFF9FDFB),
                        ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter)),
                    height: 100,
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            height: 200,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.white, Color(0xFFC8EBD9)],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(width: 7),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'عبد الله عبد الرحمن عبد الله',
                      style: TextStyle(
                          color: Color(0xFF0C5263),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 15),
                    RichText(
                        text: TextSpan(
                            text: '% 73',
                            style: TextStyle(
                                color: Color(0xFF0C5263),
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                            children: [
                          TextSpan(text: '    '),
                          TextSpan(text: 'أنت تحفظ من الجزء')
                        ])),
                    SizedBox(height: 5),
                    RichText(
                        text: TextSpan(
                            text: '% 65',
                            style: TextStyle(
                                color: Color(0xFF0C5263),
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                            children: [
                          TextSpan(text: '    '),
                          TextSpan(text: 'أنت تحفظ من القرآن')
                        ]))
                  ],
                ),
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.3),
                        blurRadius: 10,
                        offset: Offset(0, 6),
                      ),
                    ],
                  ),
                  child: CircularPercentIndicator(
                    radius: 170,
                    percent: 65 / 100,
                    progressColor: Color(0xFF00CCF2),
                    backgroundColor: Colors.transparent,
                    animation: true,
                    circularStrokeCap: CircularStrokeCap.round,
                    lineWidth: 13,
                    center: Text(
                      '65%',
                      style: TextStyle(
                          color: Color(0xFF0C5263),
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
