import 'package:flutter/material.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class LibraryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'المكتبة والأنشطة',
          style: Theme.of(context).textTheme.headline1,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Center(
          child: Stack(
            children: [
              CostumeContainer(
                marginVerticalBottom: 100,
                marginVertical: 70,
                marginHorizontal: 20,
                color: Colors.white,
                radiusBottom: 30,
                radiusTop: 30,
                child: Text('ملفات ، مستندات ، تطبيقات '),
              ),
              Positioned(
                bottom: 80,
                right: 100,
                left: 100,
                child: FloatingActionButton(
                  onPressed: () {},
                  child: Icon(Icons.add),
                  backgroundColor: Theme.of(context).primaryColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
