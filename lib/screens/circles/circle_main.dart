import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:yaseen_app/data.dart';
import 'package:yaseen_app/screens/circles/circle_children.dart';
import 'package:yaseen_app/screens/circles/circle_data.dart';
import 'package:yaseen_app/widgets/costume_container.dart';

class CircleMainScreen extends StatelessWidget {
  CircleMainScreen({Key? key}) : super(key: key);
  List<String> functionData = ['حذف', 'تعديل', 'إضافة'];

  @override
  Widget build(BuildContext context) {
    List data = [
      DataApp(
          mainText: 'حلقـة البراعــم',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleChildren(),
              ))),
      DataApp(
          mainText: 'حلقـة الـصف الرابـع',
          color: const Color(0xFFDCFCC6),
          function: () {}),
      DataApp(
          mainText: 'حلقـة الـصف الخامس',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleChildren(),
              ))),
      DataApp(
          mainText: 'حلقـة الـصف السـادس',
          color: const Color(0xFFDCFCC6),
          function: () {}),
      DataApp(
          mainText: 'حلقـة الإعـداديـة',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleChildren(),
              ))),
      DataApp(
          mainText: 'حلقـة الثانـويـة',
          color: const Color(0xFFDCFCC6),
          function: () {}),
      DataApp(
          mainText: 'حلقـة دار القــرآن',
          color: Colors.white,
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleChildren(),
              ))),
      DataApp(
          mainText: 'حلقـة منتدى الحفاظ',
          color: const Color(0xFFDCFCC6),
          function: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CircleChildren(),
              ))),
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        centerTitle: true,
        title: Text(
          'الحلقات',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: Column(
          children: [
            SizedBox(
              height: 70,
              child: ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: functionData.length,
                  itemBuilder: (context, index) => CostumeContainer(
                        function: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => CircleDataScreen(),
                            )),
                        child: Text(functionData[index],
                            style: Theme.of(context).textTheme.bodyText1),
                        color: Colors.white,
                        shadow: false,
                        width: 140,
                        marginHorizontal: 1.5,
                      )),
            ),
            const SizedBox(height: 8),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, index) => CostumeContainer(
                height: 70,
                child: Text(data[index].mainText,
                    style: Theme.of(context).textTheme.bodyText1),
                color: data[index].color,
                function: data[index].function,
                shadow: false,
              ),
            )
          ],
        ),
      ),
    );
  }
}
